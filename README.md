# PV-Monitor

Regelmäßiges Auslesen von Wechselrichter-, Stromzähler- (eHZ) und Wallbox-Werten mit Speicherung in Datenbank sowie Regelung des Überschussladens und Web-Darstellung der aktuellen Werte.

Inhaltsverzeichnis:

[[_TOC_]]

Es gibt genügend andere, umfassendere Projekte (z.B. evcc) mit fertiger Unterstützung etlicher Zähler, Wechselrichter und Wallboxen, aber meins läuft **nativ auf Synology-NAS ohne Docker**, z.B. einer recht alten DS213j.  
Es nutzt das sowieso vorhandene Python sowie die installierte PostgreSQL-DB auf dem NAS.

Aktuell für folgende Geräte implementiert:
- Hichi-IR-USB-Lesekopf für eHZ Kaifa MB310
- Wechselrichter Huawei SUN2000
- Wallbox go-eCharger 

Prinzipiell aber leicht anpassbar, sofern:
- eHZ SML-Protokoll im Push-Verfahren ausgibt
- Wechselrichter ModBus-TCP nutzt
- Wallbox per HTTP ansprechbar ist


## Installation

### eHZ-Lesekopf

Ich habe einen [Hichi-USB-Lesekopf](https://www.ebay.de/sch/i.html?_nkw=hichi+usb) benutzt. Dieser wird magnetisch vor die IR-Schnittstelle des eHZ befestigt und an einem USB-Port des NAS angeschlossen.  
Für das Auslesen sind noch Treiber nötig.

**Installation auf Synology mit DSM 6.2:** (andere vermutlich ähnlich)

Passendes Treiber-Paket als *.spk herunterladen:  
<https://synocommunity.com/package/synokernel-usbserial>  
(Schlecht erreichbar:)&nbsp; <http://www.jadahl.com>&nbsp; oder direkt&nbsp;
[USB drivers for DSM 6.2](http://www.jadahl.com/drivers_6.2/)

Dann auf der Synology-Web-Oberfläche *Paketzentrum / Manuelle Installation*
und die heruntergeladene *.spk auswählen.  
=> `/dev/ttyUSB0` wird erstellt (evtl. auch ttyUSB1, falls das andere schon exist.).

Nach jedem Reboot werden die Berechtigungen davon jedoch auf `rw-------` zurückgesetzt,
so dass ein Auslesen nur noch als root möglich wäre.  
Lösung: *Systemsteuerung/Aufgabeplaner*, dort *Erstellen/Ausgelöste Aufgabe*,  
mit Parametern&nbsp; *Benutzer:* root,&nbsp; *Ereignis:* Hochfahren,&nbsp;
*Script:* `chmod 666 /dev/ttyUSB*`

**Manueller Test:**

Konfiguration der Schnittstelle auf 9600 8N1 per:&nbsp; `stty -F /dev/ttyUSB0 9600 cs8 -cstopb -parenb raw`
(Hinweis: dies muss nach jedem Anschließen ausgeführt werden).  
Auslesen per:&nbsp; `od -x </dev/ttyUSB0`  
Da sollten grob im Sekundentakt Daten kommen, die insbesondere am Anfang jeder Datencharge `1b1b1b1b` enthalten.

**Konfiguration eHZ Kaifa MB310 für volle Datenausgabe:**

Im Normalfall bekommt man nur die Gesamt-Zählerstände in vollen kWh, was nur wenig aussagt.  
Nach Eingabe der PIN (beim VNB anfragen!) über den Knopf rechts neben dem Display, mit kurzem Drücken (ca. 15x) zu "INF" sowie "PIN" und jeweils mit langem Drücken (8-10s) INF auf on sowie PIN auf off stellen.  
Dann wird die aktuelle Leistung permanent im Display angezeigt und die IR-Ausgabe ist vollständig mit vielen Daten und auf 1/10 Wh genau.

Bei anderen Zählern wird es ähnlich gehen, manchmal ist statt Knopf eine Taschenlampe als Lichtsignal nötig - ggf. beim VNB nach der Doku fragen.



### PostgreSQL

(basiert auf [http://www.sondregronas.com/managing-postgresql-on-a-synology-server/](http://www.sondregronas.com/managing-postgresql-on-a-synology-server/))

*(TODO)*

Siehe Datei `sql/dbCreate.sql`, per `psql` (via ssh möglich) oder z.B. Anwendung *pgAdmin* ausführen:

1. als Postgres-Admin:&nbsp; User (*pv* sowie *pvreport*) und Database (*photovoltaik*) einrichten  
	Die Namen können prinzipiell frei gewählt werden.  
	*pvreport* ist für den rein lesenden Zugriff z.B. per Grafana gedacht.
2. als User *pv*:&nbsp; Tabellen erstellen und Rechte vergeben.



### Hauptanwendung (Python)

Die Anwendung ist für Python 3 geschrieben, Synology bietet **Python 3.8** als Standardpaket im Paket-Zentrum an. Dieses ist zu installieren (oder auch eine neuere Python3-Version).  
Anschließend sollte per ssh mit&nbsp; `python3 --version`&nbsp; die Nutzbarkeit geprüft werden.

Einige Python-Pakete sind nötig, die am einfachsten per `pip` installiert werden können.  
Ist `pip` schon installiert? Per&nbsp; `pip -V`&nbsp; oder ggf.&nbsp; `python3 -m pip -V`&nbsp; prüfen.  
Falls es fehlt, installieren per:&nbsp; `sudo python3 -m ensurepip --default-pip`&nbsp; und erneut prüfen.

Die benötigten **Python-Packages** können global installiert werden (meine Empfehlung), alternativ lokal für den aktuellen User (dann unten ohne `sudo`) oder natürlich auch per Python-`venv`.  
`sudo pip install pyserial`&nbsp;  (Serielle Schnittstelle)  
`sudo pip install smllib`&nbsp;  (SML Parser für eHZ)  
`sudo pip install pg8000`&nbsp;  (PostgreSQL driver)  
`sudo pip install pymodbus`&nbsp;  (ModBus für Wechselrichter)  
`sudo pip install weconnect`&nbsp;  (VW WeConnect API)  

Alle Dateien aus `src` inkl. Unterverzeichnissen irgendwo auf das NAS hin kopieren (wird im folgenden *PM* genannt).  
Datei `pvConfig_Template.ini` in `pvConfig.ini` umbenennen/duplizieren und darin die eigenen Werte eintragen (insbesondere bei XXX):  
*breitengrad* + *laengengrad*&nbsp; für Sonnenaufgang/-untergangermittlung  
*user* + *password*&nbsp; bei&nbsp; [Database]  
*host*&nbsp; bei&nbsp; [Inverter] + [Wallbox]

**Für einen ersten Test:**  
`cd PM`  
`python3 main.py`&nbsp;&nbsp;  (oder ggf.&nbsp; `nohup python3 main.py`)  
(beenden mit Ctrl+C bzw. `kill -INT <pid>`, pid-Ermittlung per `ps -ef | grep 'python.*main\.py'` )

**Dauerhafte Einrichtung** (d.h. Start bei jedem NAS-Boot):

- Eigenen Benutzer mit minimalen Rechten einrichten:  
	*Systemsteuerung/Benutzer*, Button *Erstellen* (z.B. "pvmonitor").  
	Nur Group *users*, sonst keinerlei Berechtigungen, bei Verzeichnis "public" ggf. explizit *kein Zugriff*.
- Login-Berechtigung für diesen erteilen:  
	*Systemsteuerung/Aufgabeplaner*, dort *Erstellen/Ausgelöste Aufgabe*,  
	mit Parametern&nbsp; *Benutzer:* root,&nbsp; *Ereignis:* Hochfahren,&nbsp;  
	*Script:*&nbsp; `sed -i  '/^pvmonitor:/s~/sbin/nologin~/bin/sh~'  /etc/passwd`
- Dateien aus `src` in ein Unterverzeichnis (z.B. `pvm`) des User-homes kopieren oder Link zum Ablageort erstellen.  
	(z.B. `ln -s /volume1/XXX/PV-Monitor/src pvm` &nbsp; Leseberechtigungen nicht vergessen.)
- Automatischer Start beim Boot unter dem neuen User:
	*Systemsteuerung/Aufgabeplaner*, dort *Erstellen/Ausgelöste Aufgabe*,  
	mit Parametern&nbsp; *Benutzer:* **pvmonitor**,&nbsp; *Ereignis:* Hochfahren,&nbsp;
	*Script:* (die `~` sind nötig, da das pwd irgendwo in den Synology-Strukturen liegt)
	
	    cd ~/pvm
	    python3 main.py >>~/pvMonitor.log


### Webanwendung

Diese ist in der Python-Hauptanwendung enthalten, im Unterverzeichnis "web". Sie wird auch über deren Webserver bereitgestellt, so dass prinzipiell nichts zu tun ist.

Lediglich in `web/js/config.js` ist die Host bzw. IP und Port der Python-Hauptanwendung einzutragen, also Name bzw. IP des NAS und den Port aus `pvConfig.ini/[Web]/port`. Per Javascript werden dorthin AJAX-Requests gesendet, um die aktuellen Daten zu erhalten.

Der Aufruf erfolgt per `http://<Name|IP des NAS>:<Port>/web/pvMonitor.html` also z.B. `http://192.168.178.20:8043/web/pvMonitor.html`.

Alternativ könnten die Dateien unter `web` auch im sowieso laufenden Apache eingebunden werden - effektiv bringt dies aber keinen Vorteil. Man kann lediglich die Zugriffsbeschränkungen des Apache nutzen.  
Als weitere Alternative könnten die Dateien unter `web` auch alle auf's Smartphone kopiert werden und lokal geöffnet werden.



## Prinzipien

Die Python-Anwendung liest eHZ, Wechselrichter und Wallbox regelmäßig aus und speichert die Daten in die PostgreSQL-Datenbank.  
Weiterhin wird eine HTTP-API angeboten, um aktuelle Daten und die kurzfristige Historie abzufragen (JSON-Format) sowie die Wallbox-Regelung zu beeinflussen. Details siehe Datei `main.py` bei `class PVJsonReqHandler`.

Die Webanwendung nutzt diese HTTP-Schnittstelle.

**Daten und Abfragehäufigkeit**

Generell ist ein Kompromiss zwischen Belastung der Geräte, Netzwerk, Datenvolumen in der Datenbank und Genauigkeit der Erfassung zu machen. Die Standardeinstellungen sind aus meiner Sicht dafür sinnvoll gewählt, aber anpassbar (per `pvConfig.ini`).

- eHZ und Wechselrichter werden sekündlich abgefragt (einstellbar: *readInterval*, 0-5s sinnvoll).
- Wallbox wird nur alle 10s abgefragt und auch geregelt (einstellbar: *readInterval*, 5-20s sinnvoll, schneller empfiehlt go-e nicht).  
	Wenn kein Fahrzeug angeschlossen ist sogar nur jede Minute (einstellbar: *readIntervalInactive*).
- In die Datenbank werden wichtige Daten (Momentanleistung, PV-Ertrag) sekündlich abgespeichert (bzw. im *readInterval*, DB-Tabellen *_highfreq*),  
	andere Daten (Spannung, Phasenabweichung, o.ä.) aber nur minütlich (einstellbar: *mediumSaveInterval*)  
	oder beim Wechselrichter ein paar sogar nur täglich (DB-Tabelle *_daily*).

Die aktuellen Daten und kurzzeitige Historie werden im RAM gespeichert, so dass die Anfragen über die HTTP-API nicht aus der DB lesen brauchen. 

**Wallbox-Regelung**

(TODO)



## Nutzung

Aufruf im Webbrowser per `http://<Name|IP des NAS>:<Port>/web/pvMonitor.html`

Web-Darstellung auf dem Smartphone (Layout ist dafür ausgelegt):

![](doc/Smartphone.png)

Automatischer Refresh oben einstellbar, initial 30s. Ein manueller Refresh ist alternativ/zusätzlich möglich.  
Ist die Webseite nicht sichtbar (andere App oder Tab im Vordergrund), so wird nicht automatisch aktualisiert (Netzwerklast reduzieren). Sobald sie wieder sichtbar ist, wird direkt aktualisiert (siehe auch Stand-Anzeige oberhalb der Zählerwerte).  
Regelmodus bei der Wallbox kann eingestellt werden. Wenn aktuell geladen wird, so gibt es auch Stopp- bzw. Start-Buttons.

Die Diagramme sind so breit, wie es die Seitenbreite erlaubt, aber max. 24 Stunden bzw. 20 Minuten.  
Automatische Anpassung der Breite beim Drehen des Smartphones:

![](doc/Smartphone_quer.png)

&nbsp;  
Am PC mit großem Bildschirm (nutzt Breite für Diagramme direkt aus):

![](doc/Web_PC.png)

---

Auswertungen der Daten z.B. mit Grafana:

![](doc/Grafana1.png)

![](doc/Grafana2.png)



## ToDo / Ideen

- SVG statt Canvas
- größere Diagramme bei Klick darauf
- ggf. Optimierung/Verfeinerung der Wallbox-Regelung
- Eigenverbrauchs-Ermittlung besser, so dass weniger "Gezappel" bei WR-Ertragsänderung
- Logging besser
- ID.4 startet laden nach Ladepause (alw=0) manchmal nicht wieder



## Externe Referenzen / Dokus

[USB-Driver Synology](https://synocommunity.com/package/synokernel-usbserial) oder <http://www.jadahl.com>  
[Gute Beschreibung von SML](https://www.stefan-weigert.de/php_loader/sml.php)  
[Python SML-Package](https://pypi.org/project/smllib/)  
[Python ModBus-Package](https://github.com/riptideio/pymodbus/)  
[Python PostgreSQL-Driver](https://github.com/tlocke/pg8000)  
[Python WeConnect](https://github.com/tillsteinbach/WeConnect-python)  


## License
The project and source is public domain, free use.
