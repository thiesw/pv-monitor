"""Diverse Hilfsmethoden"""

from datetime import datetime, date, time, timedelta
from time import sleep
import urllib.request, urllib.error
import logging
from typing import Iterable, Optional


LOG = logging.getLogger(__name__)


def truncMs(dt: datetime, msToAdd: int = 0) -> datetime:
	"""ms aus datetime entfernen, so dass nur ganze Sekunden übrig bleiben.
	Vorher 'msToAdd' ms aufaddieren also Art von Rundung."""
	return (dt + timedelta(milliseconds=msToAdd)).replace(microsecond=0)


def deltaTime(t: time, delta: timedelta) -> time:
	"""Add delta to time only and return resulting time."""
	return (datetime.combine(datetime(2000,1,4), t) + delta).time()


def sleepUntilNextSecond(baseDT: datetime = None):
	"time.sleep() bis zum Beginn der nächsten Sekunde, ggf. bezogen auf die übergebene Basiszeit"
	now = datetime.now()
	sleepSeconds = 0
	if baseDT is None:
		sleepSeconds = (1000000 - now.microsecond) / 1000000
	elif (now - baseDT).seconds <= 0:
		sleepSeconds = (1000000 - now.microsecond) / 1000000
	#else: a second has already passed
	if sleepSeconds > 0:
		#LOG.debug("sleeping %s s  for now=%s, baseDT=%s", sleepSeconds, now, baseDT)
		sleep(sleepSeconds)


def iterSlice(coll: Optional[Iterable], start: int, end: int = 1<<31) -> list:
	"""Return list as part of iterable/collection, like iterable[start : end] but for e.g. deque.
	start + end may be negative (to start from end), then contents will be reversed.
	start + end may be larger than size of coll.
	end may be omitted like coll[start:]
	start is inclusive, end is exclusive.
	Returns empty list if coll is None or empty.
	"""
	if coll is None  or  len(coll) == 0  or  end == start:
		return []
	if end < 0:
		end = len(coll) + end
	if start >= 0:
		pos = 0
		result = []
		for v in iter(coll):
			if pos >= end:
				break
			if pos >= start:
				result.append(v)
			pos += 1
		return result
	else:
		start = len(coll) + start
		pos = len(coll)-1
		result = []
		for v in reversed(coll):
			if pos < start:
				break
			if pos < end:
				result.append(v)
			pos -= 1
		return result
	"""TEST:
	from collections import deque
	d = deque([1,2,3,4,5], maxlen = 6)
	import utils
	utils.iterSlice(d, 0, 2)
	utils.iterSlice(d, 3, 4)
	utils.iterSlice(d, 3)
	utils.iterSlice(d, -2)
	utils.iterSlice(d, -3, -1)
	"""
#end iterSlice()


def openUrl(url: str, **kwargs):  #-> urllib.response.addinfourl:
	"""Wie urllib.request.urlopen() aber HTTPError wird als normale Response mit Status (wie 404) zurückgegeben.
	"""
	try:
		return urllib.request.urlopen(url, **kwargs)
	except urllib.error.HTTPError as e:
		return e
	# URLError wird normal thrown
