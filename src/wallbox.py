"""
Wallbox auslesen und Ladeleistung regeln, als separater Thread.
Derzeit nur go-eCharger V2 (mit API v1).
(evtl.: https://github.com/cathiele/goecharger/blob/master/goecharger/goecharger.py)
Info:
- "amx" nimmt nur Werte von 6-32 an, sonst wird es ignoriert
- Laden pausieren/deaktivieren ist damit also nicht möglich => alw=0 bzw. =1 nutzen.
"""
import logging, enum, math
from time import sleep
from datetime import date, datetime, timedelta
from threading import Thread, Event
from configparser import SectionProxy
from collections import deque
from typing import Union, Callable, Tuple, Optional, Iterable
from http import HTTPStatus
import urllib.request, json
import utils, database


LOG = logging.getLogger(__name__)


# Mindestladestrom (Integer!)
MIN_AMPERE: int = 6
# Max. Ladestrom
MAX_AMPERE: int = 16
# Wieviel W "Reserve" für den Eigenverbrauch einplanen?  (sollte >= 50 W, wegen Aufrunden bei Ampere)
RESERVE_WATT = 150
# Wieviel A Ladestrom (Kommazahl!) muss vorhanden sein, damit bei SONNE_PLUS trotz zu wenig Leistung geladen wird?
SONNE_PLUS_MIN_LADESTROM = 2.25				# 2.0 A = 1380 W,  2.5 A = 1725 W,  3.5 A = 2415 W
# Bei wie wenig A Ladestrom (Kommazahl!) soll selbst bei OHNE_PAUSE abgeschaltet werden? (Wert <= 0 für nie abschalten)
OHNE_PAUSE_ABSCHALTEN_UNTERHALB_A = -99		# 0.7 A = 483 W,  1.05 A = 730 W
# Alle wieviele Sekunden darf ein Schaltvorgang (Laden an/aus) maximal erfolgen?
WARTEZEIT_SCHALTVORGANG = 5*60
# Wieviele Sekunden muss genügend Energie verfügbar sein, damit das Laden (wieder) begonnen wird (enable-delay)
#WARTEZEIT_LADESTART = 2*60
# Wieviele Sekunden muss zu wenig Energie verfügbar sein, damit das Laden unterbrochen wird (disable delay)
#WARTEZEIT_LADESTOP = 30


# Name bzw. Key für DATADEF bzw. lastData (sowie Spalte in DB); "DATA_*" ist nur in lastData, nicht in DB
COL_STATUS = "Status"
COL_STROM_L1 = "Strom_L1_mA"
COL_STROM_L2 = "Strom_L2_mA"
COL_STROM_L3 = "Strom_L3_mA"
COL_LADELEISTUNG = "Ladeleistung_W"
DATA_LADEN_ERLAUBT = "Laden erlaubt?"			# 0 oder 1
DATA_LADESTROM = "Ladestrom_A"					# aktuell per amp oder amx gesetzter Soll-Wert
DATA_ZULETZT_GELADEN = "Zuletzt_geladen_Wh"
DATA_REGELMODUS = "Lade-Regelmodus"

# Map mit Key = string Parameter und Value = (DB-Spaltenname, DB-Typ, [Faktor=1] )
DATADEF = {
	"car": (COL_STATUS, "smallint"),
	"amp": (DATA_LADESTROM, "-"),					# per amp oder amx gesetzt; ist feststehender Wert, daher nicht in DB
	"alw": (DATA_LADEN_ERLAUBT, "-"),
	"nrg[0]": ("Spannung_L1_V", "smallint"),		# bei mir unpräzise, typisch nur 218-220 V (Korrekturfaktor wäre 1.064)
	"nrg[1]": ("Spannung_L2_V", "smallint"),
	"nrg[2]": ("Spannung_L3_V", "smallint"),
	"nrg[4]": (COL_STROM_L1, "smallint", 100),
	"nrg[5]": (COL_STROM_L2, "smallint", 100),
	"nrg[6]": (COL_STROM_L3, "smallint", 100),
	"nrg[11]": (COL_LADELEISTUNG, "smallint", 10),	# wird mit der ungenauen Spannung berechnet, bei mir daher zu niedrig (Strom stimmt aber recht gut)
#	"tmp": ("Temperatur_Ctrl_gC", "smallint"),		# bei mir recht konstant bei 28, "tma" ändert sich jedoch
	"tma[0]": ("Temperatur0_gC", "f"),				# Info, bei mir z.B.:  'tma': [23.88, 23.38, 23.75, 23.5, 26.13, 26.88]
	"tma[1]": ("Temperatur1_gC", "-f"),
	"tma[2]": ("Temperatur2_gC", "-f"),
	"tma[3]": ("Temperatur3_gC", "-f"),
	"tma[4]": ("Temperatur4_gC", "-f"),
	"tma[5]": ("Temperatur5_gC", "f"),
	"eto": ("Gesamtenergie_Wh", "-", 100),			# wird mit der ungenauen Spannung berechnet
}

TABLENAME = "wallbox"


# Status-Werte ("car"):
STATUS_INACTIVE = 1			# Charger not connected to vehicle (aka "ready")
STATUS_CHARGING = 2
STATUS_WAITING = 3			# Waiting for vehicle (??)
STATUS_FINISHED = 4			# charging finished, vehicle still connected

@enum.unique
class RegelModus(enum.Enum):
	# ohne Regelung
	MANUELL = 0
	# mit voller Leistung laden
	MAX = 1
	# nur bei Überschuss laden, lieber Laden unterbrechen als Netzbezug (in der Nacht gar nicht laden)
	NUR_SONNE = 2
	# wie NUR_SONNE, aber bei nur etwas zu wenig Überschuss trotzdem laden (siehe SONNE_PLUS_MIN_LADESTROM)
	NUR_SONNE_PLUS = 5
	# möglichst nur mit Überschuss laden, aber (fast) nie pausieren, dann minimal laden (d.h. 6 A = 4 kW);
	# ab kurz vor Sonnenuntergang aber Laden beenden (siehe auch OHNE_PAUSE_ABSCHALTEN_UNTERHALB_A)
	OHNE_PAUSE_BIS_SONNENUNTERGANG = 3
	# wie oben, aber ab kurz vor Sonnenuntergang mit max. Leistung voll laden
	OHNE_PAUSE_MAX = 4
#end enum


class Wallbox(Thread):
	"""Wallbox auslesen (go-eCharger).
	Relevante Attribute:
	- status (int)   Wert von "car" (siehe STATUS_*)
	- lastLadeleistung (int, W)   ist i.d.R. auf echte Leistung korrigiert
	- lastLadestroeme (deque[float], W)   Ladeströme der letzten 120s (im readInterval-Raster), nur wenn Charging
	- lastData (dict string/Bezeichnung -> int/real/double/datetime Wert)

	Überschussladen Regelung:
	- nur nötig, wenn Auto abgeschlossen, d.h. Status 2|3|4
	- siehe enum RegelModus
	- Es wird mit dem Wallbox-readInterval geregelt, typisch also 5-10 s.
	  In dieser Frequenz wird der aktuelle Wert vom eHZ und WR gelesen (d.h. lastData davon), es wird über die
	  letzten paar Werte gemittelt, um ein "Zappeln" bei starker Werteänderung zu ignorieren.
	  Falls keine aktuellen Daten vorliegen (lastData[Zeit] älter als zwei Minuten, z.B. wegen Abfragefehler),
	  wird dies als kein PV-Ertrag gewertet (d.h. Laden unterbrechen oder nur mit Minimalleistung laden).
	- Pro Regelung wird mit dem höchsten Strom geladen, so dass die Einspeisung möglichst klein ist,
	  d.h. Zähler-Momentanleistung ein geringer negativer Wert ist. Etwas "Reserve" (100-200W) wird gegeben.
	ABER:
	- Ein-/Ausschalten des Ladevorgangs (d.h. < 6A und >= 6A) wird begrenzt, so dass dies max. alle 5min passiert.
	  Dies reduziert häufiges Schalten des Relais/Schütz in der Wallbox und ggf. im Auto, was den Verschleiß reduziert.
	- Enable-Delay: (30-60s) Die PV-Anlage muss für mindestens diese Zeit die Mindestleistung (6A)
	  erzeugen/erzeugt haben, damit die Ladung wieder gestartet wird.
	  Schutz vor "Flackern" am Morgen oder Verschwinden einer Wolke.
	- Disable-Delay: (30-60s) Wird die Mindestleitung (6A) unterschritten, wird erst nach dieser Zeit das Laden
	  unterbrochen. Die Ladestrom-Reduzierung ist nicht betroffen.
	  Schutz vor kleiner Wolke oder kurzem Verbrauchspeak.
	  Das Ausmaß der Unterschreitung beeinflusst die Delay-Dauer: eine kräftige Unterschreitung muss nur
	  kürzer vorliegen, um abzuschalten; eine nur leichte Unterschreitung wird länger toleriert.
	"""
	
	def __init__(self, wbConfig: SectionProxy, stopEvent: Event, db: database.Database, 
						getLastCurrentPowersFunc: Callable[[], Iterable[int]],
						getLastProducedFunc: Callable[[], int],
						getRealVoltageFunc: Callable[[], Tuple[float,float,float]] = None):
		"""Wallbox init.
		:param getLastCurrentPowersFunc: Funktion, die Iterable/deque der letzten Momentanleistungen zurückgibt.
		:param getLastProducedFunc: Funktion, die letzte produzierte Leistung zurückgibt.
		:param getRealVoltageFunc: optional, Funktion, die 3-Tuple mit echten Spannungen zurückgibt.
		"""
		Thread.__init__(self)
		self.name = "Wallbox_" + self.name
		self._config = wbConfig
		self._readInterval = max(3, self._config.getint("readInterval", fallback=10))	# at least 3s
		self.NUM_INTERVALS_30S = math.ceil(30 / self._readInterval)
		self.NUM_INTERVALS_60S = math.ceil(60 / self._readInterval)
		self._readIntervalInactive = max(10, self._config.getint("readIntervalInactive", fallback=60))	# at least 10s
		self._baseUrl = "http://" + self._config.get("host") + ":" + self._config.get("port", "80") + "/"
		self._stopEvent = stopEvent
		self._database = db
		self._getLastCurrentPowersFunc = getLastCurrentPowersFunc
		self._getLastProducedFunc = getLastProducedFunc
		self._getRealVoltageFunc = getRealVoltageFunc
		self._regelModus = self.getDefaultRegelModus()
		self._leistungKorrekturfaktor = 1.0
		self._lastSchaltvorgangZeit = datetime.min
		self.lastLadestroeme = deque(maxlen = 2*self.NUM_INTERVALS_60S)   # letzte Soll-Ladeströme als Kommazahl
		self.status = 0
		self.lastLadeleistung = 0
		self.lastData = {}


	def _close(self):
		pass		# nothing to do here


	def getDefaultRegelModus(self) -> RegelModus:
		"""Standardregelmodus für aktuelles Datum ermitteln.
		In den sonnenarmen Monaten funktioniert reines PV-Laden nicht, dort sinnvollen Modus wählen.
		"""
		monat = date.today().month
		if monat in {12,1}:
			return RegelModus.MAX
		elif monat in {10,11,2}:
			return RegelModus.NUR_SONNE_PLUS
		return RegelModus.NUR_SONNE


	def run(self):
		LOG.info("Starting thread, using %s", self._baseUrl)
		prevStatus = 0
		try:
			while not self._stopEvent.is_set():
				try:
					# Info: bei mir (V2) ist kein "?filter" o.ä. möglich, immer alle Daten als JSON
					with urllib.request.urlopen(self._baseUrl + "status", timeout=7) as resp:
						# Info: Hier gilt immer, da sonst URLError:  resp.status == HTTPStatus.OK:
						msgTime = utils.truncMs(datetime.now(), 100)
						wbData = json.load(resp)
					#end with

					self.lastData[database.COL_ZEIT] = msgTime
					self.status = int(wbData.get("car", 0))
					cols = [database.COL_ZEIT]
					values = [msgTime]
					for (code, ddTuple) in DATADEF.items():
						index = -1
						faktor = (ddTuple[2] if len(ddTuple) >= 3 else 1)
						if (pos := code.find("[")) > 0:				# Array-Angabe
							index = int(code[pos+1 : -1])
							code = code[:pos]
						if (val := wbData.get(code)) is not None:
							try:
								if index >= 0:
									val = val[index] * faktor		# Arraywert ist bereits Zahl
								else:
									val = float(val) * faktor		# alles ist als String gegeben
								if ddTuple[1] not in ("f", "-f", "d", "-d"):
									val = int(val)
								self.lastData[ddTuple[0]] = val
								if not ddTuple[1].startswith("-"):
									cols.append(ddTuple[0])
									values.append(val)
							except (IndexError, TypeError):
								LOG.warning("Keine gültige Zahl für '%s'[%s] in '%s'", code, index, val)
						else:
							LOG.warning("Data-code '%s' not found in response.", code)

					self.lastLadeleistung = self.lastData.get(COL_LADELEISTUNG, 0)
					self.lastData[DATA_REGELMODUS] = self._regelModus.name
					if self._getRealVoltageFunc:
						# Ladeleistung mit richtiger Spannung selber ermitteln
						(v1, v2, v3) = self._getRealVoltageFunc()
						if v1 and v2 and v3:
							echteLadeleistungW = round(v1 * self.lastData.get(COL_STROM_L1, 0) / 1000  \
													+ v2 * self.lastData.get(COL_STROM_L2, 0) / 1000  \
													+ v3 * self.lastData.get(COL_STROM_L3, 0) / 1000,  0)
							if 1 < echteLadeleistungW > self.lastLadeleistung * 0.9:		# defensiv Plausibilität prüfen
								self._leistungKorrekturfaktor = echteLadeleistungW / max(1, self.lastLadeleistung)
								if self._leistungKorrekturfaktor < 0.5  or  self._leistungKorrekturfaktor > 2.0:
									self._leistungKorrekturfaktor = 1.0		# defensiv "wilde" Werte verhindern
								self.lastData["Leistung_Korrekturfaktor"] = self._leistungKorrekturfaktor
								self.lastLadeleistung = echteLadeleistungW
								values[cols.index(COL_LADELEISTUNG)] = echteLadeleistungW
						elif self._leistungKorrekturfaktor != 1.0:
							# letzten Korrekturwert nutzen, falls keine aktuellen Spannungen bekannt
							self.lastLadeleistung *= self._leistungKorrekturfaktor
							values[cols.index(COL_LADELEISTUNG)] = self.lastLadeleistung
							
					#LOG.debug("Data: %s = %s\n   %s", cols, values, self.lastData)

					if self.lastLadeleistung > 1  or  self.status == STATUS_CHARGING  or  self.status != prevStatus:
						# wenn lädt oder Statuswechsel, dann in DB
						self._database.saveData(TABLENAME, cols, values)
						if self.status != STATUS_CHARGING:
							# wenn nicht lädt, dann danach dummy-null-Row in DB
							self._database.saveData(TABLENAME, [database.COL_ZEIT], [msgTime + timedelta(milliseconds=100)])
					if self.status == STATUS_CHARGING:
						if prevStatus != STATUS_CHARGING:
							# Info: Ladeleistung ist 1-3s lang unpassend
							self.lastLadestroeme.clear()
							sollLadestrom = self.lastData.get(DATA_LADESTROM)
							LOG.info("Laden begonnen, akt. Ladeleistung = %s W, Soll = %s A", self.lastLadeleistung, sollLadestrom)
							self.lastData.pop(DATA_ZULETZT_GELADEN, None)
							if sollLadestrom == MAX_AMPERE:
								# wenn manuell an Wallbox auf max. Ampere eingestellt, dann MAX-Modus aktivieren
								self._regelModus = RegelModus.MAX
								LOG.debug("Bei Ladebeginn ist Soll auf max, also Modus MAX aktiviert.")
					elif prevStatus == STATUS_CHARGING:
						# Laden fertig, dann Energie merken (siehe oben:  und danach null-Row in DB schreiben)
						geladeneEnergieWh = int(float(wbData.get("dws", "0")) / 360 * self._leistungKorrekturfaktor)
						self.lastData[DATA_ZULETZT_GELADEN] = geladeneEnergieWh
						LOG.info("Laden beendet/unterbrochen, status=%s, allow=%s, geladen(dws) = %s Wh (Korrekturfaktor %s)", 
								self.status, self.lastData.get(DATA_LADEN_ERLAUBT), 
								geladeneEnergieWh, self._leistungKorrekturfaktor)
						#passiert im vorigen if bereits:  self._database.saveData(TABLENAME, [database.COL_ZEIT], [msgTime + timedelta(milliseconds=100)])
					if self.status == STATUS_INACTIVE  and  prevStatus != STATUS_INACTIVE:
						# wenn nichts mehr angeschlossen, dann Laden immer (wieder) erlauben und Ladestrom auf min. stellen
						LOG.debug("Inactive geworden (von Status %s), also Minimalstrom setzen", prevStatus)
						if self._regelModus == RegelModus.MAX:			# MAX nach Abkoppeln zurücksetzen
							self._regelModus = self.getDefaultRegelModus()
						self.setLadestrom(MIN_AMPERE, True)		# falls manuell auf z.B. 16A gesetzt, dies zurücksetzen
						self.setLadenErlaubt(True)
						self.lastLadestroeme.clear()
					elif prevStatus == STATUS_INACTIVE  and  self.status in (STATUS_CHARGING, STATUS_WAITING):
						# Fahrzeug frisch angeschlossen, zählt als Schaltvorgang für _regeleLadestrom()
						self._lastSchaltvorgangZeit = msgTime

					if self.status == prevStatus == STATUS_INACTIVE:
						# nichts angeschlossen, dann längere Pause
						prevStatus = self.status
						self._stopEvent.wait(timeout=self._readIntervalInactive)
					else:
						self._regeleLadestrom()
						prevStatus = self.status
						self._stopEvent.wait(timeout=self._readInterval)

				except:  #(json.JSONDecodeError, urllib.error.HTTPError, urllib.error.URLError):
					LOG.exception("Exception in Wallbox readout loop")
					sleep(2)
			#end while

		finally:
			self._close()
			LOG.info("Ending thread")
	#end run()


	def _regeleLadestrom(self):
		"""Lade-Regelung. Setzt bei Bedarf neuen Ladestrom.
		Details siehe bei class-Doku.
		"""
		TURN_OFF_LOADING = -1
		now = datetime.now()
		currLadestrom = self.lastData[DATA_LADESTROM]
		currLadenErlaubt:bool = (self.lastData.get(DATA_LADEN_ERLAUBT, 1) != 0)
		lastProducedW = self._getLastProducedFunc()
		doSetAmpere = False

		# Spezialfälle (mit direktem "return"):
		if self._regelModus == RegelModus.MAX  and  currLadestrom < MAX_AMPERE  and  self.status == STATUS_CHARGING:
			# defensiv bei MAX prüfen und korrigieren
			LOG.debug("rLS: RM.MAX aber Ladestrom zu gering (%s A), korrigiere auf max", self.lastData[DATA_LADESTROM])
			self.setLadestrom(MAX_AMPERE)
			return
		if self._regelModus in (RegelModus.MANUELL, RegelModus.MAX)  or  self.status in (STATUS_INACTIVE, STATUS_WAITING):
			# keine Regelung gewünscht oder kein Fahrzeug angeschlossen
			return
		if 10 < self.lastLadeleistung < 2000:
			# frisch angeschlossen, so dass Ladeleistung noch nicht stimmt, dann defensiv minimal einstellen
			LOG.debug("rLS: akt. Ladeleistung (%s W) klein, setze auf min", self.lastLadeleistung)
			if currLadestrom != MIN_AMPERE:
				self.setLadestrom(MIN_AMPERE)
			return
		if lastProducedW < 100  and  self._regelModus != RegelModus.OHNE_PAUSE_MAX:  # MAX+MANUELL sind oben bereits behandelt
			LOG.debug("rLS: Laden direkt abschalten, da kein WR-Ertrag (%s W) für RM %s", lastProducedW, self._regelModus)
			if currLadenErlaubt:
				# hier kein (Aus-)Schalt-Zeitschutz, da selten auftreten sollte; Einschalten ist aber zeit-geschützt
				self._lastSchaltvorgangZeit = now
				self.setLadenErlaubt(False)
				self.setLadestrom(MIN_AMPERE)		# defensiv mit min wieder starten
			return

		# normale Regelung:   (mind. Überschuss-Ampere für Laden hängt vom Regelmodus ab)
		minAmpere = MIN_AMPERE
		if self._regelModus == RegelModus.NUR_SONNE_PLUS:
			minAmpere = SONNE_PLUS_MIN_LADESTROM
		elif self._regelModus in (RegelModus.OHNE_PAUSE_MAX, RegelModus.OHNE_PAUSE_BIS_SONNENUNTERGANG):
			minAmpere = OHNE_PAUSE_ABSCHALTEN_UNTERHALB_A
			
		if lastProducedW < 100  and  self._regelModus == RegelModus.OHNE_PAUSE_MAX:
			# nach Sonnenuntergang auf max. Ladestrom (sind so am wenigsten Verluste)
			# TODO: ? Uhrzeit prüfen
			LOG.debug("rLS: setze für RM %s außerhalb WR-Laufzeit auf max", self._regelModus)
			ampere = MAX_AMPERE
		else:
			# freie Leistung ermitteln, dafür Mittelwert der letzten Momentanleistung über einige Sekunden nutzen,
			# eine steigende (d.h. fallender WR-Ertrag oder erhöhter Eigenverbrauch) aber direkt beachten 
			lastCurrPowers = utils.iterSlice(self._getLastCurrentPowersFunc(), -5)		# !! letzter Wert ist erster in Liste
			if not lastCurrPowers:
				return
			lastCurrentPower = round(sum(lastCurrPowers) / len(lastCurrPowers))
			if len(lastCurrPowers) >= 2  and  lastCurrPowers[0] > lastCurrentPower+60  and  lastCurrPowers[1] > lastCurrentPower+20:
				# letzten beide Werte sind über Mittelwert, also max. Momentanleistung (= geringster Ertrag) nehmen
				lastCurrentPower = max(lastCurrPowers)
			freieW = max(0, -lastCurrentPower + self.lastLadeleistung - RESERVE_WATT)
			
			# Ladestrom ermitteln, hier feste Spannung nehmen; Info: 230V * 3 Phasen * 16 Ampere = 11 kW
			# An Umschaltgrenze für die ganzzahligen Ampere wird leicht mal "geflackert" (bei kleinen
			# WR-Schwankungen), daher erstmal Ampere-Kommazahl nehmen und leicht auf- bzw. abrunden, um
			# den aktuellen Strom tendenziell beizubehalten.
			lastSollLadestroeme = utils.iterSlice(self.lastLadestroeme, -self.NUM_INTERVALS_60S)
			origAmpere = freieW / 230 / 3
			ampereF = origAmpere + 0.07			# erstmal leicht aufrunden; Info: 0.07 A sind ca. 48 W
			if int(ampereF) > currLadestrom  or  not currLadenErlaubt:
				ampereF -= 0.14					# Stromerhöhung, dann aber abrunden (2*0.07)
			self.lastLadestroeme.append(ampereF)
			ampere = min(MAX_AMPERE, int(ampereF))
			LOG.debug("rLS: setze auf %i A  (%.4f A, bei Mom.lstg %i und Ladelstg. %i => freie %i W)", 
						ampere, origAmpere, lastCurrentPower, self.lastLadeleistung, freieW)
			
			if ampere < MIN_AMPERE:
				if ampereF < minAmpere:
					LOG.debug("rLS: A zu gering, schalte ab (wenn möglich, für %s)", self._regelModus)
					ampere = TURN_OFF_LOADING
				else:  #elif self._regelModus in (RegelModus.OHNE_PAUSE_MAX, RegelModus.OHNE_PAUSE_BIS_SONNENUNTERGANG):
					LOG.debug("rLS: A zu gering, setze auf min (für %s)", self._regelModus)
					ampere = MIN_AMPERE
					doSetAmpere = True
		
		schaltenErlaubt = (now - self._lastSchaltvorgangZeit).total_seconds() >= WARTEZEIT_SCHALTVORGANG
		if ampere <= TURN_OFF_LOADING:
			# erst echt abschalten, wenn mehrere Soll-Ladeströme in Folge zu klein (abhängig vom
			# Ausmaß der Unterschreitung: 30s oder 60s);
			# "minAmpere" ist == MIN_AMPERE oder typischerweise ein recht kleiner Wert
			if schaltenErlaubt  and  currLadenErlaubt  \
					and  (all(a < min(minAmpere*0.8, MIN_AMPERE*0.6) for a in lastSollLadestroeme[-self.NUM_INTERVALS_30S :])  \
					      or  all(a < min(minAmpere*1.35, MIN_AMPERE) for a in lastSollLadestroeme[-self.NUM_INTERVALS_60S :]) ):
				self._lastSchaltvorgangZeit = now
				self.setLadenErlaubt(False)
			schaltenErlaubt = False		# unten sicher *nicht* wieder einschalten
			ampere = MIN_AMPERE			# sonst erstmal nur (und auch zusätzlich zum Abschalten) auf Minimalstrom reduzieren
			doSetAmpere = True
		if currLadestrom != ampere:		# nur setzen, wenn Wert geändert und gerade lädt
			if self.status == STATUS_CHARGING  or  doSetAmpere:
				self.setLadestrom(ampere)
		if ampere > 0  and  schaltenErlaubt  and  not currLadenErlaubt:
			# Laden wieder aktivieren, falls nötig (erst NACH Strom-Setzen)
			# und mind. 30s vorige sowie aktueller Soll-Ladestrom groß genug
			if all(a >= minAmpere for a in lastSollLadestroeme[-self.NUM_INTERVALS_30S :]):
				self._lastSchaltvorgangZeit = now
				self.setLadenErlaubt(True)
	#end _regeleLadestrom()
	
	
	def setLadestrom(self, ampere: int, dauerhaftSetzen: bool = False) -> bool:
		"""Ladestrom pro Phase in Ampere (temporär) setzen per "amx", nur ganze Ampere möglich.
		Mit "dauerhaftSetzen" ist auch Setzen per "amp" möglich - sollte nur selten erfolgen!
		Beachte MIN_AMPERE und MAX_AMPERE !
		Werte außerhalb 6-32 werden schlicht ignoriert.
		Laden pausieren/deaktivieren ist damit NICHT möglich => alw=0 bzw. =1 nutzen.
		:returns true bei Erfolg, false bei Fehler.
		"""
		ampStr = str(int(ampere))
		LOG.info("setLadestrom(%s)", ampere)
		try:
			# Info: liefert geändertes status-Objekt zurück
			with urllib.request.urlopen(self._baseUrl + "mqtt?payload=" + ("amp" if dauerhaftSetzen else "amx") + "=" + ampStr, timeout=7) as resp:
				if resp.status == HTTPStatus.OK:
					#respStr = resp.read().decode("utf-8")
					#if '"amp":"' + ampStr + '"' in respStr:
						return True
					#else:
					#	LOG.warning("setLadestrom(%s) zwar 200 aber Wert nicht gesetzt: %s", ampere, respStr)
					#	return False
				else:
					LOG.warning("setLadestrom(%s) liefert Nicht-200: %s", ampere, resp)
					return False
		except:  # urllib.error.URLError
			LOG.exception("setLadestrom(%s) liefert Fehler", ampere)
		return False

	#def setLadeleistung(self, watt: int) -> bool:
	#	"""Ladeleistung in Watt (temporär) setzen per "amx", wird auf ganze Ampere abgerundet (damit mind. 4140 W nötig).
	#	:returns true bei Erfolg, false bei Fehler.
	#	"""
	#	return self.setLadestrom(int(watt / 3 / 230))
	
	
	def setLadenErlaubt(self, erlaubt: bool) -> bool:
		"""Laden erlauben bzw. deaktivieren (per "alw").
		:returns true bei Erfolg, false bei Fehler.
		"""
		LOG.info("setLadenErlaubt(%s)", erlaubt)
		try:
			with urllib.request.urlopen(self._baseUrl + "mqtt?payload=alw=" + ("1" if erlaubt else "0"), timeout=7) as resp:
				if resp.status == HTTPStatus.OK:
					return True
					#TODO??    respStr = resp.read().decode("utf-8")   etc.
				else:
					LOG.warning("setLadenErlaubt(%s) liefert Nicht-200: %s", erlaubt, resp)
		except:  # urllib.error.URLError
			LOG.exception("setLadenErlaubt(%s) liefert Fehler", erlaubt)
		return False
	
	def beendeLaden(self) -> bool:
		if self.status == STATUS_CHARGING:
			return self.setLadenErlaubt(False)
		return False

	def erlaubeLaden(self) -> bool:
		return self.setLadenErlaubt(True)
	
	# performReset:  payload=rst=1
	
	
	def setRegelModus(self, modus: Union[RegelModus,int,float,str]) -> bool:
		"""Regelmodus setzen, bei MAX auch den Ladestrom.
		:param modus: als enum RegelModus, int, float, or string
		:returns true bei erlaubtem Wert, false bei Fehler.
		"""
		LOG.info("setRegelModus(%s, %s)", modus, type(modus))
		try:
			if isinstance(modus, (int, float)):
				modus = RegelModus(modus)
			elif isinstance(modus, str):
				modus = RegelModus[modus.upper()]
			self._regelModus = modus
			self.lastData[DATA_REGELMODUS] = modus.name		# da Aktualisierung insbes. bei Inactive recht langsam erfolgt
			if modus == RegelModus.MAX:
				self.setLadestrom(MAX_AMPERE)
				self.setLadenErlaubt(True)
			elif modus != RegelModus.MANUELL  and  self.status != STATUS_CHARGING:
				self.setLadestrom(MIN_AMPERE)		# für Start des nächsten Ladevorgangs zurücksetzen (insbes. falls auf MAX stehen sollte)
			return True
		except (ValueError, KeyError, AttributeError):
			LOG.warning("Invalid parameter value for setRegelModus(): '%s' (%s)", modus, type(modus))
			return False
		
#end class



def printCreateTables():
	database.printCreateTable(TABLENAME, DATADEF, "Wallbox Daten mit normaler/mittlerer Frequenz (~ 10 - 60 s).")
