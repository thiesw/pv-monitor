"""
Wechselrichter auslesen, als separater Thread.
Derzeit nur: Huawei 30KTL per ModBus TCP.
"""
import logging
from time import sleep
from datetime import date, datetime, timedelta
from collections import deque
from threading import Thread, Event
from configparser import SectionProxy
from typing import Iterable, Tuple
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.register_read_message import ReadRegistersResponseBase
from ehz import LAST_POWER_SIZE
import utils, database, sunset


LOG = logging.getLogger(__name__)

#aus ehz übernehmen:  LAST_POWER_SIZE = 20*60			# (AUCH für eHZ anpassen!)

# Name bzw. Key für DATADEF bzw. lastData (sowie teilweise Spalte in DB)
DATA_NIGHT_SLEEP = "Nachtmodus"
COL_PRODUCED_POWER = "Leistung_AC_W"

# special value for "Teiler" in DATADEF
_EPOCH_TO_TIMESTAMP = -99

# Strings bei mir:  1: OS = Ost-sued (17),  2: ON = Ost-nord (17),  3: G = Gaube (8),  5: Cp = Carport (10),  7: W = West (19)

# Map mit Key = int register address und Value = (DB-Spaltenname, DB-Typ, [Teiler=1, [Anzahl Register=1]] )
DATADEF_HIGHFREQ = {
	32080: (COL_PRODUCED_POWER, "i", 1, 2),
	32016: ("Spannung_ON_V", "f", 10),
	32017: ("Strom_ON_mA", "i", 0.100),
	32018: ("Spannung_OS_V", "f", 10),
	32019: ("Strom_OS_mA", "i", 0.100),
	32020: ("Spannung_G_V", "f", 10),
	32021: ("Strom_G_mA", "i", 0.100),
	# String4 nicht benutzt
	32024: ("Spannung_Cp_V", "f", 10),
	32025: ("Strom_Cp_mA", "i", 0.100),
	# String6 nicht benutzt
	32028: ("Spannung_W_V", "f", 10),
	32029: ("Strom_W_mA", "i", 0.100),
	# String8 nicht benutzt
}
DATADEF = {
	32064: ("Leistung_DC_W", "i", 1, 2),
	#32066,67,68: Spannung zw. L1/L2 etc.
	32069: ("Spannung_L1_V", "f", 10),
	32070: ("Spannung_L2_V", "f", 10),
	32071: ("Spannung_L3_V", "f", 10),
	32072: ("Strom_L1_mA", "i", 1, 2),
	32074: ("Strom_L2_mA", "i", 1, 2),
	32076: ("Strom_L3_mA", "i", 1, 2),
	#32078: Peak_Leistung_am_Tag, siehe daily
	#32080 Leistung_AC, siehe HighFreq
	32082: ("Blindleistung_AC_VAr", "i", 1, 2),		# "Var" bzw. "VAr" sind Voltampere-reaktiv
	#32084: Power factor
	32085: ("Netzfrequenz_Hz", "f", 100),
	32086: ("Wirkungsgrad", "f", 100),
	32087: ("Temperatur_intern_gC", "f", 10),
	32088: ("Isolationswiderstand_kOhm", "i"),		# TODO: evtl. unnötig, da nur einmal am Tag ermittelt??
	32089: ("Device_Status", "-"),					# see page 10 of ModBus manual
	32090: ("Fault_Code", "-"),						# values undefined
	#32091: startup, siehe daily
	#32093: shutdown, siehe daily
	#32106: total accumulated energy, siehe daily
	32114: ("Daily_energy_Wh", "i", 0.100, 2),
}
DATADEF_DAILY = {		# wird täglich nach Sonnenuntergang gespeichert
	32078: ("Peak_Leistung_am_Tag_AC_W", "i", 1, 2),
	32091: ("Startup_time", "timestamptz", _EPOCH_TO_TIMESTAMP, 2),		# epoch seconds
	32093: ("Shutdown_time", "timestamptz", _EPOCH_TO_TIMESTAMP, 2),
	32106: ("Total_accumulated_energy_Wh", "i", 0.100, 2),
}
# Liste mit Tuples (Startreg.addr, Anzahl Register), MUSS/sollte obiges komplett abdecken
READ_REGISTERS = [ (32016, 7*2), (32064, 95-64), (32106, 116-106) ]

TABLENAME = "inverter"
TABLENAME_HIGHFREQ = TABLENAME + "_highfreq"
TABLENAME_DAILY = TABLENAME + "_daily"

# Minuten vor/nach Sonnenauf/untergang, während der noch ausgelesen/protokolliert wird
SUNSET_SAFETY_MINUTES = 10


class Inverter(Thread):
	"""Wechselrichter auslesen.
	Relevante Attribute:
	- lastProducedPowers (deque[int]): letzte Werte der erzeugten Leistung in W, letzte Wert ([-1]) ist der neueste;
			ggf. reversed() und iter() nutzen
	- lastData (dict string/Bezeichnung -> int/real/double/datetime Wert)
	"""

	def __init__(self, modbusConfig: SectionProxy, stopEvent: Event, db: database.Database):
		Thread.__init__(self)
		self.name = "WR_" + self.name
		self._config = modbusConfig
		self._readInterval = self._config.getint("readInterval", fallback=1)
		self._mediumSaveInterval = self._config.getint("mediumSaveInterval", fallback=60)
		self._stopEvent = stopEvent
		self._database = db
		self._modbusClient = ModbusTcpClient(self._config["host"], self._config.getint("port"))
		self.lastProducedPowers = deque([0], maxlen=LAST_POWER_SIZE)
		self.lastData = {}


	def _close(self):
		if self._modbusClient is not None:
			self._modbusClient.close()


	def run(self):
		LOG.info("Starting thread")
		unitModbus = self._config.getint("unit", fallback=1)
		lastMediumSaveTime = datetime.min
		lastDailySaveDate = date.min
		now = datetime.now()
		(sonnenAufgang, sonnenUntergang) = sunset.sonnenAufgangUntergang(now.date(), SUNSET_SAFETY_MINUTES)
		self.lastData["Sonnenaufgang"] = sonnenAufgang
		self.lastData["Sonnenuntergang"] = sonnenUntergang
		LOG.debug("Sonnenaufgang/untergang am %s (mit %s min Reserve): %s / %s", 
				now.date(), SUNSET_SAFETY_MINUTES, sonnenAufgang, sonnenUntergang)
		# gar nicht nötig, einmalig Readout durchlaufen und am Ende "schlafen":
		#		if now.time() >= sonnenUntergang:   lastDailySaveDate = now.date()
		self._modbusClient.connect()
		sleep(1)			# WICHTIG, sonst "[Input/Output] Modbus Error"
		try:
			regData = []	# wegen Exception-handling hier bereits erstellen/init.
			while not self._stopEvent.is_set():
				try:
					if self._modbusClient.connect():		# macht reconnect bei Bedarf
						# alle ModBus-Register auslesen (Info: braucht auf DS213j 70-160ms pro read_holding_registers)
						readStartTime = datetime.now()
						regData = []
						for regRead in READ_REGISTERS:
							#LOG.debug("Start reading registers %s", regRead[0])
							# Info: eine Exception wird als Rückgabewert statt per "raise" zurückgegeben
							modbusResponse = self._modbusClient.read_holding_registers(regRead[0], regRead[1], unit=unitModbus)
							if isinstance(modbusResponse, ReadRegistersResponseBase):
								regData.append(modbusResponse.registers)
							else:   #if isinstance(modbusResponse, (ExceptionResponse, ModbusException)):
								LOG.warning("Fehler bei read_holding_registers(%s..): %s", regRead[0], modbusResponse)
								regData.append([])
								self._modbusClient.close()		# vorsichtshalber reconnect
								sleep(2)
								self._modbusClient.connect()
								sleep(1)
						#LOG.debug("Finished reading registers.")

						msgTime = utils.truncMs(readStartTime, 100)		# Achtung: kann damit non-unique werden, aber siehe sleepUntilNextSecond()
						msgDate = msgTime.date()
						doMediumSave = (msgTime - lastMediumSaveTime).total_seconds() >= self._mediumSaveInterval
						#fest:  doDailySave = (msgTime.hour >= 23  and  lastDailySaveDate != msgDate)
						doDailySave = (msgTime.time() > sonnenUntergang  and  lastDailySaveDate != msgDate)

						def readoutDataDef(dataDef, cols, values):
							for (ddRegAddr, ddTuple) in dataDef.items():
								if ddRegAddr < 0:
									continue
								teiler = (ddTuple[2] if len(ddTuple) >= 3 else 1)
								numRegs = (ddTuple[3] if len(ddTuple) >= 4 else 1)
								value = None
								einRegValuesLeer = False
								for ((regStart, regCount), regValues) in zip(READ_REGISTERS, regData):
									if len(regValues) == 0:			# falls durch Modbus-Fehler gar nichts gelesen worden ist
										einRegValuesLeer = True
									elif regStart <= ddRegAddr <= regStart + len(regValues):
										value = regValues[ddRegAddr - regStart]
										if numRegs > 1:				# (?) TODO: bisher nur 1 und 2 (16/32 Bit) unterstützt
											value = (value << 16) + regValues[ddRegAddr+1 - regStart]
											if value >= 1<<31:		# 2er-Komplement ermitteln (z.B. für Blindleistung relevant)
												value -= 1<<32
										elif value >= 1<<15:		# es gibt auch u16, aber die sind alle < 32767, daher generell machen
											value -= 1<<16			# (ist z.B. bei Strom oder Temperatur manchmal relevant)
								if value is not None:
									if teiler == _EPOCH_TO_TIMESTAMP:
										value = datetime.utcfromtimestamp(value)
									else:
										value /= teiler
										if ddTuple[1] not in ("f", "-f", "d", "-d"):
											value = int(value)
									self.lastData[ddTuple[0]] = value
									if not ddTuple[1].startswith("-"):
										cols.append(ddTuple[0])
										values.append(value)
								elif not einRegValuesLeer:
									LOG.info("ModBus-Register %s wurde nicht ausgelesen, prüfe READ_REGISTERS !", ddRegAddr)
						#end readoutDataDef()

						# alle Registerdaten auswerten und ggf. in DB speichern
						self.lastData[database.COL_ZEIT] = msgTime
						cols = [database.COL_ZEIT]
						values = [msgTime]
						readoutDataDef(DATADEF_HIGHFREQ, cols, values)
						self.lastProducedPowers.append(self.lastData[COL_PRODUCED_POWER])
						self._database.saveData(TABLENAME_HIGHFREQ, cols, values)
						cols = [database.COL_ZEIT]
						values = [msgTime]
						readoutDataDef(DATADEF, cols, values)
						if doMediumSave:
							self._database.saveData(TABLENAME, cols, values)
							lastMediumSaveTime = msgTime
						cols = [database.COL_DATUM]
						values = [msgDate]
						readoutDataDef(DATADEF_DAILY, cols, values)
						if doDailySave:
							self._database.saveData(TABLENAME_DAILY, cols, values)
							lastDailySaveDate = msgDate
							
						now = datetime.now()
						if now.time() > sonnenUntergang  or  now.time() < sonnenAufgang:
							# Tagesende (für den WR), also bis kurz vor dem nächsten Sonnenaufgang warten;
							# auch bei erstem Start nach Mitternacht vor Sonnenaufgang.
							if now.time() > sonnenUntergang:
								# null-Werte als Tagesabschluss schreiben, damit diese nicht verbunden werden
								cols = [database.COL_ZEIT]
								values = [msgTime + timedelta(milliseconds=100)]
								self._database.saveData(TABLENAME_HIGHFREQ, cols, values)
								self._database.saveData(TABLENAME, cols, values)
								tomorrow = now.date() + timedelta(days=1)
							else:
								tomorrow = now.date()			# sind bereits in den (frühen) Morgenstunden
							self.lastData[DATA_NIGHT_SLEEP] = True
							(sonnenAufgang, sonnenUntergang) = sunset.sonnenAufgangUntergang(tomorrow, SUNSET_SAFETY_MINUTES)
							self.lastData["Sonnenaufgang"] = sonnenAufgang
							self.lastData["Sonnenuntergang"] = sonnenUntergang
							# lastProducedPowers komplett mit 0 füllen, damit keine Reste verbleiben (denn
							# es wird während der Nachtwartezeit nicht fortgeschrieben!)
							self.lastProducedPowers.extend(0 for i in range(len(self.lastProducedPowers)-1))
							tdeltaSunrise = datetime.combine(tomorrow, sonnenAufgang) - now
							LOG.info("Sonnenuntergang, Tagesdaten gespeichert, sleep() bis %s => %s", sonnenAufgang, tdeltaSunrise)
							self._modbusClient.close()
							if not self._stopEvent.wait(timeout=tdeltaSunrise.total_seconds()):
								self.lastData.pop(DATA_NIGHT_SLEEP, None)
								# vorsichtshalber reconnect mit kurzer Wartezeit
								self._modbusClient.connect()
								sleep(1)

					else:
						LOG.warning("Could not connect to ModBus")
						sleep(1)

					if self._readInterval is not None  and  self._readInterval > 0:
						self._stopEvent.wait(timeout=self._readInterval)
					else:
						utils.sleepUntilNextSecond(msgTime)

				except:
					LOG.exception("Exception in Inverter readout loop, regData = %s", regData)
					self._modbusClient.close()
					sleep(2)
					self._modbusClient.connect()
					sleep(1)					# WICHTIG, sonst "[Input/Output] Modbus Error"
			#end while

		finally:
			self._close()
			LOG.info("Ending thread")
	#end run()


	def getLastProducedPowers(self) -> Iterable[int]:
		return self.lastProducedPowers

	def getLastProduced(self) -> int:
		"""Zuletzt produzierte Energie, Mittelwert über einige Sekunden."""
		lpp = utils.iterSlice(self.lastProducedPowers, -9)
		if not lpp:
			return 0
		return round(sum(lpp) / len(lpp))

	def getLastVoltage(self) -> Tuple[float,float,float]:
		if self.lastData[DATA_NIGHT_SLEEP]:
			return (None, None, None)
		else:
			return (self.lastData.get("Spannung_L1_V"), self.lastData.get("Spannung_L2_V"), self.lastData.get("Spannung_L3_V"))

#end class



def printCreateTables():
	database.printCreateTable(TABLENAME_HIGHFREQ, DATADEF_HIGHFREQ, "Wechselrichter-Daten mit hoher Frequenz (~ 1 Hz).")
	database.printCreateTable(TABLENAME, DATADEF, "Wechselrichter-Daten mit normaler/mittlerer Frequenz (~ 10 - 60 s).")
	database.printCreateTable(TABLENAME_DAILY, DATADEF_DAILY, "Wechselrichter-Daten mit täglicher Frequenz.", True)
