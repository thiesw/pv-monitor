"""Umgang mit Sensordaten"""

from datetime import datetime, date, time, timedelta
import logging
from typing import Dict
import utils, database


LOG = logging.getLogger(__name__)


# Map mit Key = string Parameter und Value = (DB-Spaltenname, DB-Typ)
DATADEF_SD = {
	"id": ("Ort", "varchar(50)"),
	"temp": ("Temperatur", "f"),
	"humi": ("Luftfeuchtigkeit", "f"),
}
TABLENAME_SD = "sensorData"

COL_STAND = "Stand_m3"
COL_TICKS = "Ticks"
DATADEF_GAS = {
	"a": (COL_STAND, "d"),
	"b": (COL_TICKS, "i"),
}
TABLENAME_GAS = "gas"


class Sensors():
	"""
	Sensordaten verarbeiten und persistieren.
	"inaktive" Klasse, keine Threads.
	"""
	
	def __init__(self, db: database.Database): 
		self._database = db
		try:
			# letzten Wert aus DB lesen
			self._gasMeter, self._lastGasCount = db.selectSingle(
					f"SELECT {COL_STAND}, {COL_TICKS} FROM {TABLENAME_GAS} ORDER BY zeit DESC LIMIT 1")
		except:
			LOG.exception("Fehler beim Auslesen des Gasstands aus DB, beginne bei 0.")
			self._lastGasCount = 0
			self._gasMeter = 0.0			# im m³
		LOG.debug("Gasstand, initiale Werte: %0.3f, %d ticks", self._gasMeter, self._lastGasCount)


	def gasTick(self, counter: int):
		"""Eine (ggf. mehrere) Umdrehung das Gaszählers ist erfolgt, laufende Anzahl Ticks in counter.""" 
		dataTime = utils.truncMs(datetime.now(), -20)
		ticksOccurred = 1			
		if counter > self._lastGasCount:		#NEIN:  and  self._lastGasCount > 0:
			ticksOccurred = counter - self._lastGasCount
		self._lastGasCount = counter;
		# ein Tick sind 0,010 m³ = 10 l = ca. 0,095 (~0,1) kWh L-Gas
		# bei 3kW Minimalleistung des Brenners kommt mind. alle 1/30 h (= 2min) ein Tick
		#  (real sind es 5 kW = 1800 U/min, also mind. alle 72 s bei Brennerbetrieb)
		#  (bei Max.last von 12 kW alle 30s ein Tick, bei 22 kW alle 16s)
		# der Tick kommt beim 0-Duchgang der letzten Ziffer, daher gegebenen Stand mit
		# mehr Kommastellen darauf abschneiden; "krumme" 0.0048 wegen floating point Toleranz
		self._gasMeter = round(self._gasMeter - 0.0048 + ticksOccurred / 100, 2)
		#LOG.debug("gasTick %d => %0.3f", ticksOccurred, self._gasMeter)
		cols = [database.COL_ZEIT, COL_STAND, COL_TICKS]
		values = [dataTime, self._gasMeter, self._lastGasCount]
		self._database.saveData(TABLENAME_GAS, cols, values)
	#end gasTick()


	def setGasMeter(self, value: float):
		"""Zählerstand setzen, als Korrektur."""
		dataTime = utils.truncMs(datetime.now())
		LOG.info("setGasMeter %f (old = %f)", value, self._gasMeter)
		self._gasMeter = value
		# bei schnellen Ticks kann die Sekunde hier doppelt vorkommen und dann
		# ein UNIQUE error in der DB entstehen - kann ignoriert werden
		cols = [database.COL_ZEIT, COL_STAND, COL_TICKS]
		values = [dataTime, self._gasMeter, self._lastGasCount]
		self._database.saveData(TABLENAME_GAS, cols, values)
	#end setGasMeter()
	

	def sensorData(self, data: Dict) -> bool:
		"""Sensordaten in DB speichern. data ist ein parse_qs()-Dict."""
		dataTime = utils.truncMs(datetime.now(), -50)
		#LOG.debug("sensorData %s", data)
		cols = [database.COL_ZEIT]
		values = [dataTime]
		for key, val in data.items():
			try:
				if key in DATADEF_SD:				# nur gewünschte Parameter akzeptieren
					key = DATADEF_SD[key][0]		# und in Spaltennamen wandeln
					if isinstance(val, list):		# parse_qs()-Dict hat eine Liste der Werte, 1. nehmen
						val = val[0]
					# Wert möglichst zu int, float und zuletzt string machen
					try:
						val = int(val)
					except ValueError:
						try:
							val = float(val)
						except ValueError:
							val = str(val)
					values.append(val)
					cols.append(key)
			except:
				LOG.exception("Fehler beim Parsen von " + str(key) + "/" + str(val))
		#end for
		if len(cols) > 1:
			#LOG.debug("  -> %s = %s", cols, values)
			self._database.saveData(TABLENAME_SD, cols, values)
		return True
	#end sensorData()

#end class


def printCreateTables():
	database.printCreateTable(TABLENAME_SD, DATADEF_SD, "Sensor-Daten")
	database.printCreateTable(TABLENAME_GAS, DATADEF_GAS, "Gas-Zählerstand")
