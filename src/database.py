import queue, logging
from threading import Thread, Event
from configparser import SectionProxy
import pg8000.dbapi
from typing import Sequence, Tuple, Any, Dict


LOG = logging.getLogger(__name__)

# üblicher Name der Zeit-Spalte
COL_ZEIT = "Zeit"
# üblicher Name der Datum-Spalte (ohne Zeit, daily-Werte)
COL_DATUM = "Datum"

def printCreateTable(tabName: str, dataDef: Dict[Any,Tuple[str,str]], comment:str = None, useDate = False):
	"""Print "create table" and optionally "comment on".
	dataDef has as values a tuple with at least two values: columm name and data type
	with special values "f" = float/real (~6 decimal digits), "d" = double (~15 decimal digits),
	"i" = integer32 and "-" to ignore value (i.e. just for readout/lastData, not for DB).
	"""
	print("CREATE TABLE " + tabName + " (\n\t"
			+ (COL_DATUM + " date" if useDate else COL_ZEIT + " timestamptz")
			+ "  primary key")
	for (col, dtype, *_) in dataDef.values():
		if dtype.startswith("-"):
			continue
		elif dtype == "d":
			dtype = "float8"
		elif dtype == "f":
			dtype = "real"
		elif dtype == "i":
			dtype = "integer"
		print("\t, " + col + "  " + dtype)
	print(");")
	if comment is not None:
		print("COMMENT ON TABLE " + tabName + " IS '" + comment + "';\n")
#end printCreateTable()


class Database(Thread):
	"""Database interface with async. writing to DB."""

	def __init__(self, configDB: SectionProxy, stopEvent: Event):
		Thread.__init__(self)
		self.name = "Database_" + self.name
		self._config = configDB
		self._batchSaveInterval = self._config.getint("batchSaveInterval", fallback=120)
		self._stopEvent = stopEvent
		self._conn = None		# type: pg8000.dbapi.Connection
		self._queue = queue.SimpleQueue()
		pg8000.dbapi.paramstyle = "qmark"		# see https://peps.python.org/pep-0249/#paramstyle
	#end __init__


	def _connect(self):
		"""Connect to DB if not already connected"""
		# note: not multi-threaded, one connection per thread
		if self._conn is None:
			self._crsr = None
			self._conn = pg8000.dbapi.connect(host=self._config["host"], port=self._config.getint("port"),
							user=self._config["user"], password=self._config["password"],
							database=self._config["database"])
			self._conn.autocommit = True		# ist hier immer benötigt
			LOG.info("Connection established: %s", self._conn)
		if self._crsr is None:
			self._crsr = self._conn.cursor()
	#end _connect()


	def _close(self):
		"""Close DB connection and cursor"""
		if self._crsr is not None:
			try:
				self._crsr.close()
			except:
				pass		# ignore close-errors
			self._crsr = None
		if self._conn is not None:
			try:
				self._conn.close()
			except:
				pass		# ignore close-errors
			self._conn = None
	#end _close()


	def selectSingle(self, sql: str):
		"""Execute Select-SQL and return single row."""
		self._connect()
		self._crsr.execute(sql)
		return self._crsr.fetchone() 


	def run(self):
		LOG.info("Starting thread")
		self._connect()
		sql = ""
		params = []
		try:
			while not self._stopEvent.is_set():
				if self._batchSaveInterval > 0:
					self._stopEvent.wait(timeout=self._batchSaveInterval)
				try:
					(sql, params) = self._queue.get(timeout=5)
					self._connect()
					dbErrorOccurred = False
					try:
						while True:		# process all SQLs currently inside queue
							try:
								#LOG.debug("Executing SQL:  %s  with  %s", sql, params)
								self._crsr.execute(sql, params)
								#autocommit:  self._conn.commit()			# commit each single SQL as they are independant
							except pg8000.exceptions.DatabaseError:
								LOG.exception('Database error for "%s"  with  %s', sql, params)
								self._conn.rollback()
								dbErrorOccurred = True
							(sql, params) = self._queue.get_nowait()
					except queue.Empty:
						pass	# fine, all SQLs processed
					if dbErrorOccurred:
						self._close()		# defensively close + reopen db connection on any error
				except queue.Empty:
					pass		# no SQL in queue, 5s-timeout to just check for stop
				except pg8000.exceptions.DatabaseError:
					LOG.exception('Database connection error, wait 1min + retry')
					self._stopEvent.wait(timeout=60)

		finally:
			self._close()
			LOG.info("Ending thread")
	#end run()


	def saveData(self, tableName: str, columns: Sequence[str], values: Sequence):
		"""To be called from outside: async. execute an SQL Insert statement"""
		# float value for integer columns yields in error '22P02: invalid input syntax for integer',
		# so put these values directly into SQL which is fine!?!
		valuesSql = []
		params = []
		sqlStr = "INSERT INTO " + tableName + " (" + ",".join(columns) + ") VALUES ("
		for v in values:
			if isinstance(v, (int, float)):
				valuesSql.append(str(v))
			else:
				valuesSql.append("?")
				params.append(v)
		sqlStr += ",".join(valuesSql) + ")"
		#LOG.debug("saveData():  %s  with  %s", sqlStr, params)
		self._queue.put( (sqlStr, params) )
	#end saveData()

#end class
