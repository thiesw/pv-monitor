/** insbes. Daten-Lesen und in Webseite eintragen */
"use strict";

// config.js MUSS vorher eingebunden sein!

// unit-strings as part of value label
const UNITS = ["V",  "mA", "A",  "VAr", "Var",  "W", "kW", "Wh", "kWh",  "Hz", "gC", "kOhm", "pct", "km", "km/h", "min"];
const SELECTED = "selected";		// CSS-class

// Keys für lastData[]
const DATA_MOMENTANLEISTUNG = "Momentanleistung_W";
const DATA_LADELEISTUNG = "Ladeleistung_W";
const DATA_REGELMODUS = "Lade-Regelmodus";
const DATA_LADEN_ERLAUBT = "Laden erlaubt?";

// Wallbox-Statuswerte
const STATUS_INACTIVE = 1,  STATUS_CHARGING = 2,  STATUS_WAITING = 3,  STATUS_FINISHED = 4;

// Breite der Diagramm, für Ermittlung der Anzahl Werte
let gCountersWidth = 100, gPowersWidth = 100;
const LEGEND_WIDTH_POWERS = 20;			// Pixelbreite der Legende, MUSS zu Canvas-Breite passen (in *.html)
const LEGEND_WIDTH_COUNTERS = 30;
const MAX_VALUES_POWER = 20*60;			// maximale Anzahl verfügbarer Werte (MUSS zu Python-Impl. passen!)
const MAX_VALUES_COUNTER = 24*60/5;
const DEFAULT_WIDTH_POWERS = LEGEND_WIDTH_POWERS + 6*60;		// initial Breite (6 Minuten, 380px)


const LOCALE = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;

let gAutoRefreshTimer;
let gErrorTimer;

let lastWallboxPower = 0;		// globale Var. (arg) für Daten von refreshData() -> drawPowers()



/** Remove given CSS-class from all elements of given tag/selector inside given parent elem. */
function removeClassFromAll(parentElem, tag, className) {
	parentElem.querySelectorAll(tag).forEach(el => el.classList.remove(className));
}

function logError(error, logTextPrefix) {
	console.log(logTextPrefix + error + " (" + error.fileName + ": " + error.lineNumber + ")");
}



/** Für Aufruf am Ende von body. */
function initPvManager() {
	adjustPosAndSize();
	const elemRefresh = document.getElementById("refresh");
	const rectRefresh = elemRefresh.getBoundingClientRect();
	// refresh-Buttons rechtsbündig
	elemRefresh.style.left = Math.round(DEFAULT_WIDTH_POWERS - rectRefresh.width) + "px";

	refreshAll();
	setAutoRefresh(30, document.getElementById("initialAutorefresh"), false);
	window.addEventListener("resize", resizeListener);
	document.addEventListener("visibilitychange", visibilityListener);
}


function resizeListener() {
	let newWidth = window.innerWidth;
	// nur bei ernsthafter Breitenänderung (wird sonst zu häufig gemacht, insbesondere mobil mit Addresszeile ein-/ausblenden)
	if (!resizeListener.lastWindowWidth  ||  Math.abs(newWidth - resizeListener.lastWindowWidth) > 50) {
		adjustPosAndSize();
		refreshPowers();
		refreshCounters();
		resizeListener.lastWindowWidth = window.innerWidth;
	}
}

/** Adjust position and sizes of some elements (what is not possible with CSS). */
function adjustPosAndSize() {
	// Fensterbreite minus rechtem body-Rand und ~Scrollbar, die meist erst später (nach createLastDataRows) entsteht
	const winWidth = window.innerWidth - 4 - 20;		// oder:  document.documentElement.clientWidth;
	const elemPowers = document.getElementById("diagramPowers");
	const rectPowers = elemPowers.getBoundingClientRect();
	const elemCounters = document.getElementById("diagramCounters");
	const rectCounters = elemCounters.getBoundingClientRect();
	
	// Info: manche left- oder width-Werte sind Kommazahlen, daher "round/floor"
	// Breite Counters passend (passend zu Powers bzw. max. 24h)
	if (winWidth > DEFAULT_WIDTH_POWERS + 150) {
		// Leistungsgraph auf Fensterbreite, aber auf volle Minuten gerastert
		gPowersWidth = LEGEND_WIDTH_POWERS + Math.min(MAX_VALUES_POWER, Math.floor((winWidth - LEGEND_WIDTH_POWERS - rectPowers.left) / 60) * 60);
		gCountersWidth = Math.min(LEGEND_WIDTH_COUNTERS + MAX_VALUES_COUNTER, Math.floor(winWidth - rectCounters.left));
	} else {
		gPowersWidth = DEFAULT_WIDTH_POWERS;
		gCountersWidth = Math.round(rectPowers.left + gPowersWidth - rectCounters.left);
	}
	elemPowers.width = gPowersWidth;
	document.getElementById("anzMin").innerText = Math.round((gPowersWidth - LEGEND_WIDTH_POWERS) / 60);
	elemCounters.width = gCountersWidth;
	// "anzStd" wird in drawCounters() gemacht
}


/** Anzeige aktualisieren, wenn wieder sichtbar wird. */
function visibilityListener() {
	console.log(new Date().toLocaleTimeString() + " visibility changed: " + document.visibilityState);
	if (document.visibilityState === "visible") {
		refreshAll();
	}
}


/** Fehlermarker anzeigen, mit Text als Tooltip */
function showError(txt) {
	const elem = document.getElementById("error")
	if (elem) {
		elem.title = txt;
		elem.innerText = "!!! " + txt.slice(0, 40);
		elem.classList.remove("hidden");
		if (gErrorTimer) {
			clearTimeout(gErrorTimer);
		}
		gErrorTimer = setTimeout(() => { hideError(); gErrorTimer = undefined; }, 10000);
	} 
}

function hideError() {
	document.getElementById("error").classList.add("hidden");
}


/** Watt-Wert ausgeben, ggf. in kWh wandeln. Ist null-sicher. */
function formatW(data, key, kWgrenze = 5000) {
	if (data) {
		let val = data[key]
		if (val === undefined) {
			return "---";
		} else if (Math.abs(val) >= kWgrenze) {
			return (val / 1000).toLocaleString(LOCALE, {maximumFractionDigits:1, minimumFractionDigits:1}) + "\xa0kW";
		}
		return val.toLocaleString(LOCALE, {maximumFractionDigits:0}) + "\xa0W";
	} else {
		return "---";
	}
}


/** Aktuelle Werte aktualisieren. Ist null-sicher. */
function updateCurrentData(data) {
	if (data.eHZ) {
		let elem = document.getElementById("mLeistung");
		elem.innerText = formatW(data.eHZ, DATA_MOMENTANLEISTUNG);
		let lstg = data.eHZ[DATA_MOMENTANLEISTUNG]
		if (lstg < -100) {
			elem.style.color = "#009900";
		} else if (lstg > 100) {
			elem.style.color = "#cc0000";
		} else {  // auch bei undefined
			elem.style.color = "#666";
		}
		document.getElementById("verbrauch").innerText = formatW(data.eHZ, "Eigenverbrauch_W");
		let zeitStr = data.eHZ["Zeit"];
		if (zeitStr === undefined) {
			zeitStr = "???"
		} else {
			zeitStr = zeitStr.slice(zeitStr.indexOf("T")+1)
		}
		document.getElementById("zeit").innerText = zeitStr;
	}
	document.getElementById("ertrag").innerText = formatW(data.Inverter, "Leistung_AC_W");
}


function updateWallboxStatus(wbData) {
	const elem = document.getElementById("wbStatus");
	const st = wbData["Status"];
	const ladenErlaubt = wbData[DATA_LADEN_ERLAUBT];
	let displayStop = "none";
	let displayAllow = (ladenErlaubt  ?  "none"  :  "initial");
	lastWallboxPower = 0;		// erstmal zurücksetzen
	if (st === STATUS_INACTIVE) {
		elem.className = "wbKein";
		elem.innerText = "(inaktiv, nichts angeschlossen)";
	} else if (st === STATUS_CHARGING) {
		elem.className = "wbCharging";
		let stromA = (wbData["Strom_L1_mA"] + wbData["Strom_L2_mA"] + wbData["Strom_L3_mA"]) / 3 / 1000;
		lastWallboxPower = wbData[DATA_LADELEISTUNG]
		elem.innerHTML = "Lädt mit " + formatW(wbData, DATA_LADELEISTUNG, 1000)
				+ " \xa0 <span style='font-weight:normal'>(" + formatW(wbData, DATA_LADELEISTUNG, 99999)
				+ " / " + stromA.toLocaleString(LOCALE, {maximumFractionDigits:1}) + "\xa0A)</span>";
		displayStop = "initial";
		displayAllow = "none";
	} else if (st === STATUS_FINISHED) {
		const geladen = wbData["Zuletzt_geladen_Wh"];
		let txt = "Fertig"
		if (ladenErlaubt) {
			elem.className = "wbFinished";
		} else {
			elem.className = "wbPausiert";
			txt = "Pausiert"
		}
		elem.innerText = txt + (geladen  ?  " \xa0 (geladen: " + geladen.toLocaleString(LOCALE) + "\xa0Wh)"  :  "")
	} else if (st === STATUS_WAITING) {
		elem.className = "wbWaiting";
		elem.innerText = "angeschlossen";
	} else {
		elem.className = ""
		elem.innerText = "?? " + st + " ??"
	}
	
	document.getElementById("stopWb").style.display = displayStop;
	document.getElementById("allowWb").style.display = displayAllow;
	
	document.getElementById("wbStatusInfo").innerText = "Soll-Ladestrom: " + wbData["Ladestrom_A"]
			+ " A, \xa0 Laden " + (ladenErlaubt ? "erlaubt" : "-deaktiviert-");
}


function selectRegelmodusButton(regelmodus) {
	if (regelmodus) {
		let wbElem = document.getElementById("wallbox");
		let elem = wbElem.querySelector("button[data-modus='" + regelmodus + "']");
		if (elem  &&  !elem.classList.contains(SELECTED)) {
			removeClassFromAll(wbElem, "button", SELECTED);
			elem.classList.add(SELECTED);
		}
	}
}


function updateCarStatus(carData) {
	let elem = document.getElementById("carDate");
	elem.innerText = "(" + new Date(carData["Zeit"]).toLocaleString(LOCALE,
			{month:"numeric", day:"numeric", hour:"numeric", minute:"numeric", second:"numeric"})
			.replace(",", "") + ")";
	
	elem = document.getElementById("ladestand");
	let pct = carData["Ladestand_pct"];
	elem.innerText = pct + " %";
	if (pct > 70) {
		elem.className = "gut";
	} else if (pct > 40) {
		elem.className = "mittel";
	} else {
		elem.className = "niedrig";
	}
	
	elem = document.getElementById("ladestatus");
	let kw = carData["Ladeenergie_kW"];
	if (kw > 0) {
		elem.className = "charging";
		elem.innerHTML = kw + " kW, noch " + carData["Restliche_Ladezeit_min"] + " min"
				+ "&nbsp;&nbsp;<span class='ladeInfo'>(" + carData["Ladestatus"] + ")</span>"
	} else {
		elem.className = "";
		elem.innerHTML = "<span class='ladeInfo'>" + carData["Ladestatus"] + "</span>"
	}
	
	elem = document.getElementById("klima");
	elem.innerText = carData["Klimastatus"]
}


/** table-tr für lastData-Einträge erstellen, inkl. Text"verschönerung" und Zahl-Formatierung. */
function createLastDataRows(title, data) {
	let txt = "<tr><td colspan='2' class='ldTitle'><b>" + title + "</b></td></tr>";
	if (data) {
		for (let [key, val] of Object.entries(data)) {
			let unit = "";
			let pos = key.lastIndexOf("_");
			if (pos > 1) {
				unit = key.slice(pos+1);
				if (UNITS.includes(unit)) {
					unit = unit.replace("gC", "°C");
					unit = unit.replace("pct", "%");
					key = key.slice(0, pos);
				} else {
					unit = "";
				}
			}
			if (typeof(val) === "string") {
				if (val.slice(0,4) >= 2000)  val = val.replace("T", ", ");		// probably date-time, so replace "T"-separator
			} else if (typeof(val) !== "number") {
				// nichts, Umwandlung in String passiert unten
			} else if (unit === "mA"  &&  val >= 100) {
				val /= 1000;
				unit = "A";
				val = "<b>" + val.toLocaleString(LOCALE, {maximumFractionDigits: (val < 10 ? 2 : 1), minimumFractionDigits: 1}) + "</b>";
			} else if (unit === "Wh"  &&  val >= 10000) {
				let fullVal = val.toLocaleString(LOCALE, {maximumFractionDigits: 0});
				val /= 1000;
				unit = "kWh";
				val = "<b title='" + fullVal + " Wh'>" 
					+ val.toLocaleString(LOCALE, {maximumFractionDigits: (val < 100 ? 1 : 0)}) + "</b>";
			} else {
				val = "<b>" + val.toLocaleString(LOCALE) + "</b>";
			}
			key = key.replace(/_/g, " ");
			txt += "<tr><td>" + key + "</td><td>" + val + "\xa0" + unit + "</td></tr>";
		} // for
	}
	return txt;
}


/** Refresh all data and button states (but not the diagrams and their data). */
function refreshData() {
	fetch(BASE_URL + "data")
		.then(response => response.json())
		.then(data => {
				hideError();
				updateCurrentData(data);

				if (data.Wallbox) {
					updateWallboxStatus(data.Wallbox);
					selectRegelmodusButton(data.Wallbox[DATA_REGELMODUS]);
				}
				if (data.Car) {
					updateCarStatus(data.Car);
				}
				
				// Detaildaten-Liste
				let ldHtml = "";
				ldHtml += createLastDataRows("eHZ (Zähler)", data.eHZ);
				ldHtml += createLastDataRows("Wechselrichter", data.Inverter);
				ldHtml += createLastDataRows("Wallbox", data.Wallbox);
				ldHtml += createLastDataRows("Auto", data.Car);
				document.getElementById("lastData").innerHTML = ldHtml;
			})
		.catch(error => {
				logError(error, "ERROR in fetch /data: ");
				showError("Fetch data: " + error);
			});
}


/** Draw powers diagram in given canvas element. "data" is dictionary "allPowers". Reads global "lastWallboxPower". */
function drawPowers(data, elemId) {
	/* TODO: use SVG
	<svg id="diagP">
		<g id="Plegend"></g>
		<g id="Pgraph"></g>
	</svg>
	getElementById("Plegend").innerHTML = "<text ...>foo</text>"
	*/
	const LEGEND_WIDTH = LEGEND_WIDTH_POWERS;
	const MINUTE_WIDTH = 60;			// Pixel für eine Minute, MUSS zu Daten passen!
	const cnvs = document.getElementById(elemId);
	const CW = cnvs.width;
	const CH = cnvs.height;
	const ctx = cnvs.getContext("2d");
	const power = []; //NEIN, die beiden anderen reichen:  data.Power || [];
	const consumed = data.Consumed || []
	const produced = data.Produced || [];
	let wallboxA = data.Wallbox || [];
	let min = 0, max = 100;			// sensible base values
	for (const v of power) {
		if (v < min)  min = v;
		if (v > max)  max = v;
	}
	for (const v of consumed) {
		//ist nie negativ:  if (v < min)  min = v;
		if (v > max)  max = v;
	}
	for (const v of produced) {
		//ist nie negativ:  if (v < min)  min = v;
		if (v > max)  max = v;
	}
	//console.log("min = " + min + "  max = " + max)
	// scale to next discrete value (e.g. full kW)
	const SCALE_RASTER = (max > 5000  ?  1000  :  500);		// min/max auf wieviele Watt runden?
	min = (min > -9  ?  0  :  Math.floor((min - 10) / SCALE_RASTER) * SCALE_RASTER);
	max = Math.ceil((max + 10) / SCALE_RASTER) * SCALE_RASTER;
	const factorY = (CH - 2) / (max - min);
	const zeroY = (min == 0  ?  CH-2  :  0 + Math.round(max * factorY));
	//console.log("min = " + min + "  max = " + max + ",  zY=" + zeroY)
	
	ctx.clearRect(0, 0, CW, CH);
	ctx.fillStyle = "#000";
	// draw zero line und kurze 0-Linie in Legende
	if (min < 0) {			// nur relevant, wenn Momentanleistung auch ausgegeben wird
		ctx.fillRect(LEGEND_WIDTH, zeroY, CW - LEGEND_WIDTH, 1);
	}
	ctx.fillStyle = "#808080";
	ctx.fillRect(9, zeroY, LEGEND_WIDTH - 12, 1); 
	// Achsen-Beschriftung
	ctx.fillStyle = "#000";
	ctx.font = "9px sans-serif";
	ctx.fillText( (max/1000).toLocaleString(LOCALE, {maximumFractionDigits:1}), 1, 8)
	ctx.fillText( (max/2/1000).toLocaleString(LOCALE, {maximumFractionDigits:1}), 1, CH/2 + 3)
	ctx.fillStyle = "#333333";
	ctx.fillText("kW", 2, 19)
	ctx.fillStyle = "#000";
	ctx.fillText("0", 2, zeroY+1)
	
	// Raster für Minuten
	ctx.strokeStyle = "#cccccc";
	ctx.setLineDash([1,2]);
	let maxMinute = Math.floor(CW / MINUTE_WIDTH)
	for (let m = 0; m < maxMinute; m++) {
		let x = LEGEND_WIDTH + m*MINUTE_WIDTH + 0.5;
		ctx.beginPath();
		ctx.moveTo(x, 0.5);
		ctx.lineTo(x, CH + 0.5);
		ctx.stroke();
	}
	// horiz. Mittellinie
	ctx.beginPath();
	ctx.moveTo(LEGEND_WIDTH - 2.5, CH/2 - 0.5);
	ctx.lineTo(CW + 0.5, CH/2 - 0.5);
	ctx.stroke();
	// 1/4 + 3/4 Linie in Höhe
	ctx.setLineDash([1,4]);
	ctx.beginPath();
	ctx.moveTo(LEGEND_WIDTH + 0.5, CH/4 - 0.5);
	ctx.lineTo(CW + 0.5, CH/4 - 0.5);
	ctx.moveTo(LEGEND_WIDTH + 0.5, CH*3/4 - 0.5);
	ctx.lineTo(CW + 0.5, CH*3/4 - 0.5);
	ctx.stroke();
	ctx.setLineDash([]);
	
	/* nur Pixel:
	function drawGraph(points, color) {
		ctx.fillStyle = color;
		let x = X0;
		for (let y of points) {
			y = zeroY - Math.round(y * factorY);
			ctx.fillRect(x, y, 1, 1);
			x++;
		}
	} // drawGraph() */
	function drawGraph(points, color) {
		// +0.5 for sharp lines
		ctx.beginPath();
		let x = LEGEND_WIDTH + 0.5;
		for (let y of points) {
			y = 0.5 + zeroY - Math.round(y * factorY);
			//if (x < 1) {
			//	ctx.moveTo(x, y);
			//} else {
				ctx.lineTo(x, y);
			//}
			x += 1;
		}
		ctx.strokeStyle = color;
		ctx.stroke();
	} // drawGraph()
	
	drawGraph(power, "#999900");
	drawGraph(produced, "#00cc00");
	drawGraph(consumed, "#cc0000");
	//TODO wallboxA
	
	if (lastWallboxPower) {
		ctx.fillStyle = "#ff9900";
		let y = zeroY - Math.round(lastWallboxPower * factorY);
		ctx.fillRect(CW-10, y, 10, 1); 
	}
}


/** Refresh powers diagram data. Reads global "gPowersWidth". */
function refreshPowers() {
	fetch(BASE_URL + "allPowers?maxnum=" + (gPowersWidth - LEGEND_WIDTH_POWERS))
		.then(response => response.json())
		.then(data => drawPowers(data, "diagramPowers"))
		.catch(error => {
				logError(error, "ERROR in fetch /allPowers: ");
				showError("Fetch lastPowers: " + error);
			});
}


/** Draw counters diagram in given canvas element. "data" is dictionary "lastCounters". */
function drawCounters(data, elemId) {
	const LEGEND_WIDTH = LEGEND_WIDTH_COUNTERS,  SCALE_RASTER = 500;		// max auf wieviele Wh (auf)runden?
	const HOUR_WIDTH = 60*60 / (data.Intervall || 60);
	const netzbezug = data.Netzbezug || []
	if (!netzbezug) {
		return;			// keine Daten, also nichts machen
	}
	const einspeisung = data.Einspeisung || [];
	const cnvs = document.getElementById(elemId);
	const CW = cnvs.width;
	const CH = cnvs.height;
	const ctx = cnvs.getContext("2d");
	
	document.getElementById("anzStd").innerText = Math.floor((CW - LEGEND_WIDTH) / HOUR_WIDTH);

	// Skalierung für beide Werte ermittlen
	// min beginnt in Anzeige bei 0, so dass die Steigerung sichtbar wird
	let min = 999999999, max = 0;
	for (const v of netzbezug) {
		if (v < min)  min = v;
		if (v > max)  max = v;
	}
	// scale to next discrete value (e.g. full kWh)
	const minNB = min;
	const maxNB = Math.ceil((max-min + 10) / SCALE_RASTER) * SCALE_RASTER;
	const factorNB = (CH - 2) / maxNB;

	min = 999999999, max = 0;
	for (const v of einspeisung) {
		if (v < min)  min = v;
		if (v > max)  max = v;
	}
	const minES = min;
	const maxES = Math.ceil((max-min + 10) / SCALE_RASTER) * SCALE_RASTER;
	const factorES = (CH - 2) / maxES;
	
	ctx.clearRect(0, 0, CW, CH);
	ctx.font = "10px sans-serif";
	ctx.fillStyle = "#008000";
	ctx.fillText( (maxES/1000).toLocaleString(LOCALE, {maximumFractionDigits:1}), 1, 9)
	ctx.fillStyle = "#990000";
	ctx.fillText( (maxNB/1000).toLocaleString(LOCALE, {maximumFractionDigits:1}), 1, 19)
	ctx.fillStyle = "#000";
	ctx.fillText("kWh", 2, 30)
	ctx.fillText("0", 2, CH-2)
	
	// Raster für Stunden
	ctx.strokeStyle = "#cccccc";
	//ctx.setLineDash([1,2]);
	let maxHour = Math.floor((CW-LEGEND_WIDTH) / HOUR_WIDTH)
	for (let h = 1; h <= maxHour; h++) {
		let x = CW - h*HOUR_WIDTH + 0.5;
		ctx.setLineDash( (maxHour-h) % 4 == 0  ?  [3,1]  :  [1,2]);
		ctx.beginPath();
		ctx.moveTo(x, 0.5);
		ctx.lineTo(x, CH + 0.5);
		ctx.stroke();
	}
	// horiz. Mittellinie
	ctx.setLineDash([1,1]);
	ctx.beginPath();
	let y = Math.round(CH/2) - 0.5;
	ctx.moveTo(LEGEND_WIDTH-5 + 0.5, y);
	ctx.lineTo(CW + 0.5, y);
	ctx.stroke();
	// 1/4 + 3/4 Linie in Höhe
	ctx.setLineDash([1,4]);
	ctx.beginPath();
	y = Math.round(CH/4) - 0.5;
	ctx.moveTo(LEGEND_WIDTH + 0.5, y);
	ctx.lineTo(CW + 0.5, y);
	y = Math.round(CH*3/4) - 0.5;
	ctx.moveTo(LEGEND_WIDTH + 0.5, y);
	ctx.lineTo(CW + 0.5, y);
	ctx.stroke();
	// kurze 0-Linie in Legende
	ctx.setLineDash([]);
	ctx.beginPath();
	y = CH-1.5
	ctx.moveTo(12 + 0.5, y);
	ctx.lineTo(LEGEND_WIDTH-8 + 0.5, y);
	ctx.stroke();
	
	function drawGraph(points, min, factor, color) {
		ctx.fillStyle = color;
		let x = CW - points.length;   //wenn von links begonnen werden soll:  LEGEND_WIDTH;
		for (let y of points) {
			y = CH-2 - Math.round((y - min) * factor);		// unten 1 Pixel zum Rand frei lassen (für Wert 0)
			ctx.fillRect(x, y, 1, 1);
			x++;
		}
	} // drawGraph()
	
	drawGraph(netzbezug, minNB, factorNB, "#cc0000");
	drawGraph(einspeisung, minES, factorES, "#00cc00");
}


/** Refresh counters diagram data. Reads global "gCountersWidth". */
function refreshCounters() {
	fetch(BASE_URL + "lastCounter?maxnum=" + (gCountersWidth - LEGEND_WIDTH_COUNTERS))
		.then(response => response.json())
		.then(data => drawCounters(data, "diagramCounters"))
		.catch(error => {
				logError(error, "ERROR in fetch /lastCounters: ");
				showError("Fetch lastCounters: " + error);
			});
}


/** Refresh data + diagrams. */
function refreshAll() {
	refreshData();
	refreshPowers();
	refreshCounters();
}


function clearRefresh() {
	if (gAutoRefreshTimer) {
		clearInterval(gAutoRefreshTimer);
	}
	gAutoRefreshTimer = undefined;
}

/** Perform single manual refresh and don't touch auto-refresh. */
function manualRefresh() {
	//NEIN: clearRefresh();
	refreshAll();
}

/** Callback of refresh timer. */
function timedRefresh() {
	if (document.visibilityState == "visible") {
		refreshAll();
	}
}

/** Set auto-refresh interval, 0 for none, also perform immediate refresh. */
function setAutoRefresh(secs, buttonElem, doRefresh = true) {
	clearRefresh();
	const wasSelected = buttonElem.classList.contains(SELECTED);
	removeClassFromAll(buttonElem.parentElement, "button", SELECTED);
	if (!wasSelected  &&  secs > 0) {
		gAutoRefreshTimer = setInterval(timedRefresh, 1000*secs);
		buttonElem.classList.add(SELECTED);
		if (doRefresh) {
			refreshData();
			//NEIN:  refreshAll();
		}
	}
}


/** Wallbox-Regelmodus setzen. */
function setRegelModus(buttonElem) {
	removeClassFromAll(document.getElementById("wallbox"), "button", SELECTED);
	fetch(BASE_URL + "setWbMode?mode=" + buttonElem.dataset.modus)
		.then(response => {
				buttonElem.classList.add(SELECTED);
			})
		.catch(error => {
				logError(error, "ERROR in setWbMode: ");
				showError("setWbMode: " + error);
			});
}


/** Wallbox-Laden abschalten. */
function stopWallbox() {
	const elem = document.getElementById("wbStatus");
	elem.innerText = "...stopped, warte auf Aktualisierung...";
	elem.className = "wbKein";
	fetch(BASE_URL + "stopWb")
		.then(response => { refreshData(); })
		.catch(error => {
				logError(error, "ERROR in stopWb: ");
				showError("stopWb: " + error);
			});
}

/** Wallbox-Laden erlauben. */
function allowWallbox() {
	fetch(BASE_URL + "allowWb")
		.then(response => { refreshData(); })
		.catch(error => {
				logError(error, "ERROR in allowWb: ");
				showError("allowWb: " + error);
			});
}
