# PV-Analyse und Regelung
# in diesem Module auch WebServer/Request-Bearbeitung.

# Benötigt Modules:
# - pg8000  (PostgreSQL driver)
# - smllib
# - pySerial
# - pymodbus

import sys, os, logging
from datetime import date, datetime, time #, timedelta
from time import sleep
from collections import deque
from threading import Event
from configparser import ConfigParser
from http.server import BaseHTTPRequestHandler, HTTPServer, HTTPStatus
from urllib.parse import urlparse, parse_qs
import mimetypes, email.utils, shutil
import json
import sunset, database, wallbox as wallboxMod, ehz as ehzMod, inverter as inverterMod, car as carMod, sensors as sensorsMod


LOG = logging.getLogger(__name__)

#global, see main():  ehz, inverter, wallbox, car, sensors


class PVJsonReqHandler(BaseHTTPRequestHandler):
	"""Handle requests, return JSON with data depending on requested path.
	Path: "/data"   or e.g. "/data?ehz&wb"
		optional query parameters: "all", "ehz", "inv[erter]", "wb"|"wallbox", "car"; empty means "all"
		Return lastData of all or just the requested elements.
	Path: "/lastPower",  "/lastConsumed",  "/lastProduced",  "/allPowers"  with optionally: ?maxnum=###
		Return last values of current power, consumed power, produced power, or all powers.
		allPowers includes last 120s of charging currents in A of wallbox), only if currently charging.
	Path: "/lastCounter"  with optionally: ?maxnum=###
		Return last values of counter values.
	Path: "/setWbMode?mode=##"   int/string wallbox.RegelModus
		Set regulation mode of wallbox charging.
	Path: "/stopWb"
		Stop loading (disallow it until car is disconnected).
	Path: "/allowWb"
		Allow loading (e.g. if currently stopped/disabled).
	Path: "/stopClima"
		Stop clima of car.
	path: "/setGasMeter?m3=123.456"
		Set gas meter to float value of "m3" parameter. dot as decimal separator.
	Path: "/web/*"   serve files
	POST:
	Path: "/sendGasTick?tick=###"
	Path: "/sendTemp?id=XX&temp=##.##&humi=##.##"
	"""
	
	class JsonEncoder(json.JSONEncoder):
		"""Add date/time + deque serializing."""
		def default(self, obj):
			if isinstance(obj, (datetime, date, time)):
				return obj.isoformat()
			elif isinstance(obj, deque):
				return list(obj)
			return super().default(obj)
	#end class


	def _writeJson(self, obj):
		self.send_response(HTTPStatus.OK)
		self.send_header("Content-type", "application/json")
		self.send_header("Access-Control-Allow-Origin", "*")
		self.end_headers()
		self.wfile.write(bytes(json.dumps(obj, cls=self.JsonEncoder), "utf-8"))
		#NEIN: json.dump(respData, self.wfile)	# self.wfile needs bytes, not string/character

	def _writeString(self, txt: str):
		self.send_response(HTTPStatus.OK)
		self.send_header("Content-type", "text/plain")
		self.send_header("Access-Control-Allow-Origin", "*")
		self.end_headers()
		self.wfile.write(bytes(txt, "utf-8"))
	
	def _getIntQParam(self, reqUrl, param: str, defaultValue: int) -> int:
		"""Get int query parameter with default value on error, case-insensitive."""
		try:
			return int(parse_qs(reqUrl.query.lower()).get(param.lower())[0])
		except:		# ValueError, TypeError
			return defaultValue

	def _getMaxNum(self, reqUrl, defaultValue: int) -> int:
		maxNum = self._getIntQParam(reqUrl, "maxnum", 0)
		if maxNum > 1:
			return maxNum
		return defaultValue

	
	def do_GET(self):
		try:
			LOG.debug("GET-Request '%s' from %s", self.path, self.client_address)
			reqUrl = urlparse(self.path)
			if reqUrl.path == "/data":
				respData = {}
				queryParams = parse_qs(reqUrl.query.lower(), keep_blank_values=True)
				allData = ("all" in queryParams  or  len(queryParams) == 0)
				if allData  or  "ehz" in queryParams:
					respData["eHZ"] = ehz.lastData
				if allData  or  "inv" in queryParams  or  "inverter" in queryParams:
					respData["Inverter"] = inverter.lastData
				if allData  or  "wb" in queryParams  or  "wallbox" in queryParams:
					respData["Wallbox"] = wallbox.lastData
				if allData  or  "car" in queryParams:
					respData["Car"] = car.lastData
				if len(respData) == 0:
					self.send_error(HTTPStatus.BAD_REQUEST, "Invalid query parameters")
				else:
					self._writeJson(respData)
	
			elif reqUrl.path == "/lastPower":
				maxNum = self._getMaxNum(reqUrl, 99999)
				self._writeJson(list(ehz.lastCurrentPowers)[-maxNum :])
			elif reqUrl.path == "/lastConsumed":
				maxNum = self._getMaxNum(reqUrl, 99999)
				self._writeJson(list(ehz.lastConsumedPowers)[-maxNum :])
			elif reqUrl.path == "/lastProduced":
				maxNum = self._getMaxNum(reqUrl, 99999)
				self._writeJson(list(inverter.lastProducedPowers)[-maxNum :])
			elif reqUrl.path == "/allPowers":
				maxNum = self._getMaxNum(reqUrl, 99999)
				respData = {
						"Power": list(ehz.lastCurrentPowers)[-maxNum :],
						"Consumed": list(ehz.lastConsumedPowers)[-maxNum :],
						"Produced": list(inverter.lastProducedPowers)[-maxNum :],
						"Wallbox": list(wallbox.lastLadestroeme),		# sind nur für 120s
						"IntervallEhz": max(1, ehz._readInterval),
						"IntervallWR": max(1, inverter._readInterval),
						"IntervallWB": max(1, wallbox._readInterval),
					}
				self._writeJson(respData)
			elif reqUrl.path == "/lastCounter":
				maxNum = self._getMaxNum(reqUrl, 99999)
				respData = {
						"Netzbezug": list(ehz.lastNetzbezug)[-maxNum :],
						"Einspeisung": list(ehz.lastEinspeisung)[-maxNum :],
						"Intervall": ehzMod.LAST_COUNTER_INTERVAL,
					}
				self._writeJson(respData)
				
			elif reqUrl.path == "/setWbMode":
				queryParams = parse_qs(reqUrl.query.lower(), keep_blank_values=True)
				if (modeList := queryParams.get("mode")) is not None:
					if wallbox.setRegelModus(modeList[0]):
						self._writeString(wallbox._regelModus.name)
					else:
						self.send_error(HTTPStatus.BAD_REQUEST, 'Invalid value for "mode"')
				else:
					self.send_error(HTTPStatus.BAD_REQUEST, 'Invalid query parameters, "mode" is missing')
	
			elif reqUrl.path == "/stopWb":
				self._writeString(str(wallbox.beendeLaden()))
			elif reqUrl.path == "/allowWb":
				self._writeString(str(wallbox.erlaubeLaden()))
	
			elif reqUrl.path == "/stopClima":
				self._writeString(str(car.beendeKlima()))
	
			elif reqUrl.path == "/setGasMeter":
				queryParams = parse_qs(reqUrl.query.lower())
				if (m3 := queryParams.get("m3")) is not None:
					m3 = float(m3[0])
					sensors.setGasMeter(m3)
					self._writeString("Gas meter set to " + str(m3))
				else:
					self.send_error(HTTPStatus.BAD_REQUEST, 'Invalid query parameters, "m3" is missing')
	
			elif reqUrl.path.startswith("/web/")  or  reqUrl.path.startswith("/favicon"):
				if ".." in reqUrl.path  or  reqUrl.path.endswith("/")  or  reqUrl.path.count("/") > 3:
					# etwas Schutz vor beliebigem Dateizugriff 
					self.send_error(HTTPStatus.BAD_REQUEST, 'Invalid web-path')
				else:
					file = None
					try:
						if reqUrl.path.startswith("/web/"):
							realPath = reqUrl.path[1:]
						else:
							realPath = "web" + reqUrl.path		# e.g. favicon
						#LOG.debug("Serving '%s'", realPath) 
						file = open(realPath, 'rb')
						fs = os.fstat(file.fileno())
						self.send_response(HTTPStatus.OK)
						(mtype, _) = mimetypes.guess_type(realPath)
						self.send_header("Content-type", mtype)
						self.send_header("Content-Length", str(fs[6]))
						self.send_header("Last-Modified", email.utils.formatdate(fs.st_mtime, usegmt=True))
						self.end_headers()
						shutil.copyfileobj(file, self.wfile)
					except OSError:
						LOG.exception("Request file '%s'", reqUrl.path)
						self.send_error(HTTPStatus.NOT_FOUND, "File not found")
					finally:
						if (file):
							file.close()
				
			#elif self.path == "/exit?really=TRUE"  and  self.client_address[0] == "127.0.0.1":
			#	stopEvent.set()  +  self.server.shutdown()
			else:
				self.send_error(HTTPStatus.NOT_FOUND, "Invalid URL path")
		except:
			LOG.exception("Exception do_GET")
			self.send_error(HTTPStatus.INTERNAL_SERVER_ERROR)
	#end do_GET()


	def do_POST(self):
		try:
			LOG.debug("POST-Request '%s' from %s", self.path, self.client_address)
			reqUrl = urlparse(self.path)
			if reqUrl.path == "/sendGasTick":
				val = self._getIntQParam(reqUrl, "tick", -1)
				if val >= 0:
					sensors.gasTick(val)
					self.send_response(HTTPStatus.OK)
					self.end_headers()
				else:
					self.send_error(HTTPStatus.BAD_REQUEST, "invalid/missing value for tick")
			elif reqUrl.path == "/sendTemp":
				if sensors.sensorData(parse_qs(reqUrl.query)):
					self.send_response(HTTPStatus.OK)
					self.end_headers()
				else:
					self.send_error(HTTPStatus.BAD_REQUEST, "invalid/missing values")
			else:
				self.send_error(HTTPStatus.NOT_FOUND, "Invalid URL path")
		except:
			LOG.exception("Exception do_POST")
			self.send_error(HTTPStatus.INTERNAL_SERVER_ERROR)
	#end do_POST()


	#override
	def send_response(self, code, message=None):
		#self.log_request(code)
		self.send_response_only(code, message)
		#self.send_header('Server', self.version_string())
		self.send_header('Date', self.date_time_string())

	#override
	def log_message(self, formatStr, *args):
		LOG.info(formatStr, *args)
#end class



def main():
	"""Startup function, init. and start whole system.
	"""
	global ehz, inverter, wallbox, car, sensors

	#logging.basicConfig(filename = "zzz.log", filemode = "w", level = logging.DEBUG,
	logging.basicConfig(stream = sys.stdout, level = logging.DEBUG,
						format = "%(asctime)s %(levelname)-5s [%(threadName)-10s] %(name)s  -  %(message)s")
	logging.getLogger("pymodbus").setLevel(logging.INFO)
	logging.getLogger("weconnect").setLevel(logging.INFO)

	LOG.info("\n  vvv=====  Starting PV-Manager, processId=%s  =====vvv\n", os.getpid())
	
	config = ConfigParser()
	if config.read("pvConfig.ini") == []:
		print("No config file found, exiting.")
		sys.exit(1)
	
	if (bg := config.getfloat("Misc", "breitengrad")) is not None:
		sunset.setOrt(bg, config.getfloat("Misc", "laengengrad"))
	
	stopEvent = Event()
	
	db = database.Database(config["Database"], stopEvent)
	db.start()

	sensors = sensorsMod.Sensors(db)		# ist kein eigener Thread
	inverter = inverterMod.Inverter(config["Inverter"], stopEvent, db)
	ehz = ehzMod.Ehz(config["eHZ"], stopEvent, db, inverter.getLastProducedPowers)
	wallbox = wallboxMod.Wallbox(config["Wallbox"], stopEvent, db, ehz.getLastCurrentPowers, inverter.getLastProduced, inverter.getLastVoltage)
	car = carMod.Car(config["Car"], stopEvent, db)
	inverter.start()
	car.start()
	sleep(2)		# da folgendes teilweise abhängig von Daten ist, erste Auslesung ermöglichen
	ehz.start()
	sleep(2)
	wallbox.start()
	
	# Info: bei "localhost" ist er nur lokal zu erreichen
	webServer = HTTPServer( ("0.0.0.0", config.getint("Web", "port", fallback=8042)), PVJsonReqHandler)
	LOG.info("Starting web server '%s' %s", webServer.server_name, webServer.server_address)		# address includes port
	try:
		webServer.serve_forever(poll_interval=5)
	except KeyboardInterrupt:		# Ctrl-C  or  kill -INT <pid>  (kill -2)
		print("\n!!!!!!!!!!!!!!!!!!!!  Aborting, waiting for other threads...\n")
	finally:
		stopEvent.set()				# end all other threads
		webServer.server_close()

	LOG.info("^^^=====  End PV-Manager.\n")
#end main()


main()
