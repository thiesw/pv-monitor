"""
eHz (elektronischen Haushaltszähler) auslesen und Eigenverbrauch ermitteln, als separater Thread.
Derzeit nur serieller Port (z.B. Hichi IR USB, 9600 8N1) mit Push von SML.
SML siehe:  https://github.com/spacemanspiff2007/SmlLib
"""
import time, logging
from datetime import datetime
from collections import deque
from threading import Thread, Event
from configparser import SectionProxy
from typing import Tuple, Callable, Iterable
import serial, smllib
import utils, database


LOG = logging.getLogger(__name__)

LAST_POWER_SIZE = 20*60				# wieviele Daten (sekündliche) für Momentanleistung speichern (AUCH für WR anpassen!)
LAST_COUNTER_SIZE = int(24*60/5)	# wieviele Daten für Zählerwert (Netzbezug+Einspeisung) speichern
LAST_COUNTER_INTERVAL = 5*60		# Zeit in Sekunden zwischen gemerkten Zählerwerten

# Name bzw. Key für DATADEF bzw. lastData (sowie Spalte in DB)
COL_CURRENT_POWER = "Momentanleistung_W"
COL_CONSUMED_POWER = "Eigenverbrauch_W"
COL_NETZBEZUG = "Netzbezug_Wh"
COL_EINSPEISUNG = "Einspeisung_Wh"
COL_SPANNUNG_L1 = "Spannung_L1_V"
COL_SPANNUNG_L2 = "Spannung_L2_V"
COL_SPANNUNG_L3 = "Spannung_L3_V"

# Map mit Key = string OBIS-Code und Value = (DB-Spaltenname, DB-Typ, [Faktor=1])
DATADEF_HIGHFREQ = {
	"16.7.0": (COL_CURRENT_POWER, "i"),
	"-1": (COL_CONSUMED_POWER, "i"),					# nur für Tabellendef., wird berechnet
}
DATADEF = {
	"1.8.0": (COL_NETZBEZUG, "i"),
	"2.8.0": (COL_EINSPEISUNG, "i"),
	"14.7.0": ("Netzfrequenz_Hz", "f"),
	"32.7.0": (COL_SPANNUNG_L1, "f"),
	"52.7.0": (COL_SPANNUNG_L2, "f"),
	"72.7.0": (COL_SPANNUNG_L3, "f"),
	"31.7.0": ("Strom_L1_mA", "i", 1000),
	"51.7.0": ("Strom_L2_mA", "i", 1000),
	"71.7.0": ("Strom_L3_mA", "i", 1000),
	"81.7.1": ("Phasenabweichung_Spannung_L2", "smallint"),
	"81.7.2": ("Phasenabweichung_Spannung_L3", "smallint"),
	"81.7.4": ("Phasenabweichung_Strom_L1", "smallint"),
	"81.7.15": ("Phasenabweichung_Strom_L2", "smallint"),
	"81.7.26": ("Phasenabweichung_Strom_L3", "smallint"),
}

TABLENAME = "ehz"
TABLENAME_HIGHFREQ = TABLENAME + "_highfreq"

#Komplettauslesung mein Kaifa MB310:
# <obis: 010060320101, value: KFM>  ->  96.50.1 = KFM
# <obis: 0100600100ff, value: 0a0######d0000014###>  ->  96.1.0 = 0a0######0000014###   (Hersteller unabhängige Identifikationsnummer – Produktionsnummer)
# <obis: 0100010800ff, status: 1865988, unit: 30, scaler: -1, value: 33917627>  ->  1.8.0 = 3391762.7 Wh
# <obis: 0100020800ff, unit: 30, scaler: -1, value: 165093663>  ->  2.8.0 = 16509366.3 Wh
# <obis: 0100100700ff, unit: 27, scaler: 0, value: -8009>  ->  16.7.0 = -8009 W
# <obis: 0100200700ff, unit: 35, scaler: -1, value: 2337>  ->  32.7.0 = 233.7 V
# <obis: 0100340700ff, unit: 35, scaler: -1, value: 2327>  ->  52.7.0 = 232.7 V
# <obis: 0100480700ff, unit: 35, scaler: -1, value: 2306>  ->  72.7.0 = 230.6 V
# <obis: 01001f0700ff, unit: 33, scaler: -2, value: 1358>  ->  31.7.0 = 13.58 A
# <obis: 0100330700ff, unit: 33, scaler: -2, value: 1196>  ->  51.7.0 = 11.96 A
# <obis: 0100470700ff, unit: 33, scaler: -2, value: 1376>  ->  71.7.0 = 13.76 A
# <obis: 0100510701ff, unit: 8, scaler: 0, value: 120>  ->  81.7.1 = 120 °
# <obis: 0100510702ff, unit: 8, scaler: 0, value: 240>  ->  81.7.2 = 240 °
# <obis: 0100510704ff, unit: 8, scaler: 0, value: 207>  ->  81.7.4 = 207 °
# <obis: 010051070fff, unit: 8, scaler: 0, value: 210>  ->  81.7.15 = 210 °
# <obis: 010051071aff, unit: 8, scaler: 0, value: 208>  ->  81.7.26 = 208 °
# <obis: 01000e0700ff, unit: 44, scaler: -1, value: 499>  ->  14.7.0 = 49.9 Hz
# <obis: 010000020000, value: 312e3033>  ->  0.2.0 = 312e3033   (Firmware Version, Firmware Prüfsumme CRC , Datum)
# <obis: 0100605a0201, value: 7e2005d2>  ->  96.90.2 = 7e2005d2   (Prüfsumme - CRC der eingestellten Parameter)

# neuer SmartMeter Landis+Gyr E320 (2024):
# <obis: 010060320101, value: LGZ>, 
# <obis: 0100600100ff, value: 0a0########000######>, 
# <obis: 0100010800ff, status: 1835268, val_time: 2541137, unit: 30, scaler: -1, value: 858256>,
# <obis: 0100020800ff, val_time: 2541137, unit: 30, scaler: -1, value: 29660291>, 
# <obis: 0100100700ff, unit: 27, scaler: 0, value: 302>


class Ehz(Thread):
	"""Stromzähler auslesen.
	Relevante Attribute:
	- lastCurrentPowers  (deque[int]): letzte Werte der Momentanleistung in W, letzte Wert ([-1]) ist der neueste
	- lastConsumedPowers  (deque[int]): letzte Werte des berechneten Eigenverbrauchs W, letzte Wert ([-1]) ist der neueste
	- lastNetzbezug + lastEinspeisung  (deque[int]): letzte Zählerwerte in Wh
	- lastData (dict string/Bezeichnung -> int/real/double/datetime Wert)
	"""

	def __init__(self, serialConfig: SectionProxy, stopEvent: Event, db: database.Database, 
						getProducedFunc: Callable[[], Iterable[int]] = None):
		"""eHZ-Instanz erstellen.
		:param getProducedFunc: optional, Funktion, die die letzten produzierten Leistungen in W zurückgibt
		"""
		Thread.__init__(self)
		self.name = "eHZ_" + self.name
		self._config = serialConfig
		self._readInterval = self._config.getint("readInterval", fallback=0)
		self._mediumSaveInterval = self._config.getint("mediumSaveInterval", fallback=60)
		self._stopEvent = stopEvent
		self._database = db
		self._getProducedFunc = getProducedFunc
		self._serialConn = None		# type: serial.Serial
		self.lastCurrentPowers = deque([0], maxlen=LAST_POWER_SIZE)
		self.lastConsumedPowers = deque([0], maxlen=LAST_POWER_SIZE)
		self.lastNetzbezug = deque([], maxlen=LAST_COUNTER_SIZE)
		self.lastEinspeisung = deque([], maxlen=LAST_COUNTER_SIZE)
		self.lastData = {}


	def _close(self):
		if self._serialConn is not None:
			try:
				self._serialConn.close()
			except:
				LOG.exception("Ignored Exception on serial.close()")
			self._serialConn = None

	def _connect(self):
		"""Close and open serial connection."""
		try:
			self._close()
			self._serialConn = serial.Serial(port=self._config["device"], baudrate=9600,
						parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS,
						timeout=10)
			LOG.info("Connected: %s", self._serialConn)
			self._serialConn.reset_input_buffer()			# überhaupt nötig?
		except:
			LOG.exception("Exception in serial connect to %s", self._config["device"])


	def readSmlMessage(self) -> (bytes, datetime):
		"""Read from open serial port and return bytearray with full SML Message + datetime of received start.
		(None, None) if no message could be read.
		May be unclosed msg if longer than 5.000 bytes (i.e. no msg terminator read).
		Watches _stopEvent.
		"""
		tty = self._serialConn
		message = bytearray()
		startFound = False
		startTime = None
		numEscape = 0
		numBytes = 0
		while not self._stopEvent.is_set():
			byte = tty.read();
			if len(byte) == 0:						# i.e. read timeout
				return (None, None)
			numBytes += 1
			if startFound:
				message += byte
				if len(message) > 5000:				# limit max. size of message (typical size is ~600 bytes)
					return (message, startTime)		# return incomplete message
			elif numBytes > 5000:					# no start found within a lot of bytes
				return (None, None)
			if byte == b'\x1b':
				numEscape += 1
				if numEscape == 4:
					escData = tty.read(4)
					if len(escData) < 4:			# i.e. read timeout
						return (None, None)
					if escData == b'\x01\x01\x01\x01':
						if startFound:
							LOG.debug("Another SML-start found without msg end, returning incomplete msg.")
							return (message, startTime)
						message = b'\x1b\x1b\x1b\x1b' + escData
						startFound = True
						startTime = datetime.now()
					elif startFound  and  escData[0] == 0x1a:
						message += escData
						return (message, startTime)
					numEscape = 0
			else:
				numEscape = 0
		return (None, None)		# on stop-Event
	#end readSmlMessage()


	def run(self):
		LOG.info("Starting thread")
		errorSleepTimeSecs = 5
		lastMediumSaveTime = datetime.min
		lastCounterSaveTime = datetime.min
		message = None
		try:
			while not self._stopEvent.is_set():
				try:
					if self._serialConn is None:
						self._connect()
					#self._serialConn.reset_input_buffer()		# falls der eHZ zu schnell Daten liefert, keine alten lesen
					(message, msgTime) = self.readSmlMessage()
					if message is None:
						LOG.info("No SML message read (within timeout).")
					else:
						#LOG.debug("SML-Stream: %s", message.hex())
						sml = smllib.SmlStreamReader()
						sml.add(message)
						smlFrame = sml.get_frame()
						if smlFrame is None:
							LOG.info("No full SML frame, bytes missing.")
						else:
							errorSleepTimeSecs = 5						# soeben erfolgreich gelesen, also Fehlerwartezeit zurücksetzen
							msgTime = utils.truncMs(msgTime, 100)		# Achtung: kann damit non-unique werden, aber siehe sleepUntilNextSecond()
							doMediumSave = (msgTime - lastMediumSaveTime).total_seconds() >= self._mediumSaveInterval
							cols_hf = [database.COL_ZEIT]
							values_hf = [msgTime]
							if doMediumSave:
								cols = cols_hf.copy()
								values = values_hf.copy()
							else:
								cols = None
							self.lastData[database.COL_ZEIT] = msgTime

							def readoutDataDef(smlEntry, dataDef, cols, values):
								"""Check and readout given DATADEF. cols/values may be None (e.g. doMediumSave)."""
								if (ddTuple := dataDef.get(smlEntry.obis.obis_short)) is not None:
									faktor = ddTuple[2] if len(ddTuple) >= 3 else 1
									val = smlEntry.get_value() * faktor
									if ddTuple[1] not in ("f", "-f", "d", "-d"):
										val = int(val)
									self.lastData[ddTuple[0]] = val
									if not ddTuple[1].startswith("-")  and  cols is not None:
										cols.append(ddTuple[0])
										values.append(val)
							#end readoutDataDef()

							for smlEntry in smlFrame.get_obis():
								readoutDataDef(smlEntry, DATADEF_HIGHFREQ, cols_hf, values_hf)
								readoutDataDef(smlEntry, DATADEF, cols, values)
							currentPower = self.lastData[COL_CURRENT_POWER]
							self.lastCurrentPowers.append(currentPower)
							if (msgTime - lastCounterSaveTime).total_seconds() >= LAST_COUNTER_INTERVAL:
								self.lastNetzbezug.append(self.lastData[COL_NETZBEZUG])
								self.lastEinspeisung.append(self.lastData[COL_EINSPEISUNG])
								lastCounterSaveTime = msgTime
							if self._getProducedFunc is not None:
								# ein leichter zeitlicher Verzug ist bei den WR-Werten, manchmal wird zwei-dreimal
								# in Folge der gleiche Wert ausgegeben, so dass bei schnellen Änderungen ein
								# falsches "Gezappel" beim Eigenverbrauch entsteht, teilweise sogar unter 0.
								# Daher bei größerer Änderung des WR-Ertrags in den letzten 4 Werten einen
								# Mittelwert bilden (in Excel brauchbare Formel dafür ausprobiert).
								producedPowers = self._getProducedFunc()
								if not producedPowers:  # d.h.  len() == 0  oder None
									consumedPower = currentPower
								elif len(producedPowers) < 4  or  len(self.lastCurrentPowers) < 3:
									# zu wenig Werte verfügbar (z.B. direkt nach Start)
									consumedPower = max(0, currentPower + producedPowers[-1])
								else:
									# letzte 4 Werte, absteigend; Info: deque kann kein slice [a:b]
									producedList = utils.iterSlice(producedPowers, -3)
									minProd = min(producedList)
									maxProd = max(producedList)
									if maxProd - minProd < 200:
										# keine relevante Änderung des WR-Ertrags, also einfache Berechnung
										consumedPower = max(0, currentPower + producedPowers[-1])
									else:
										# stärkere Änderung des WR-Ertrags, dann Mittelwert  (u.a. über producedPowers[-3:])
										consumedPower = max(0, round(sum(producedList[0:3]) / 3 + (self.lastCurrentPowers[-3] + self.lastCurrentPowers[-2]) / 2))
								self.lastConsumedPowers.append(consumedPower)
								self.lastData[COL_CONSUMED_POWER] = consumedPower
								cols_hf.append(COL_CONSUMED_POWER)
								values_hf.append(consumedPower)
							self._database.saveData(TABLENAME_HIGHFREQ, cols_hf, values_hf)
							if doMediumSave:
								self._database.saveData(TABLENAME, cols, values)
								lastMediumSaveTime = msgTime
						#if message

					if self._readInterval is not None  and  self._readInterval > 0:
						self._stopEvent.wait(timeout=self._readInterval)
						self._serialConn.reset_input_buffer()			# nach (längerem) Warten alte Daten löschen
					else:
						self._serialConn.reset_input_buffer()			# relevant? VOR dem kurzen Warten
						utils.sleepUntilNextSecond(msgTime)

				except: #serial.SerialException:
					LOG.exception("Exception in eHZ readout loop, message = %s", (message.hex() if message is not None else "None") )
					self._close()
					time.sleep(errorSleepTimeSecs)
					errorSleepTimeSecs = min(5*60, errorSleepTimeSecs * 2)
					self._connect()
			#end while

		finally:
			self._close()
			LOG.info("Ending thread")
	#end run()


	def getLastCurrentPowers(self) -> Iterable[int]:
		return self.lastCurrentPowers

	def getLastVoltage(self) -> Tuple[float,float,float]:
		return (self.lastData.get(COL_SPANNUNG_L1), self.lastData.get(COL_SPANNUNG_L2), self.lastData.get(COL_SPANNUNG_L3))
	
#end class



def printCreateTables():
	database.printCreateTable(TABLENAME_HIGHFREQ, DATADEF_HIGHFREQ, "eHZ (Zähler) Daten mit hoher Frequenz (~ 1 Hz).")
	database.printCreateTable(TABLENAME, DATADEF, "eHZ (Zähler) Daten mit normaler/mittlerer Frequenz (~ 10 - 60 s).")
