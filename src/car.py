"""
Auto auslesen, als separater Thread.
Derzeit nur VW ID (WeConnect-API).
Info:
- nicht zu oft connecten und Daten abfragen!
"""
import logging, enum, math
from time import sleep
from datetime import date, datetime, timedelta
from threading import Thread, Event
from configparser import SectionProxy
from typing import Union, Callable, Tuple, Optional, Iterable
from weconnect import weconnect
import utils, database


LOG = logging.getLogger(__name__)

COL_TEXT = "FullText"			# wird nur in DB gespeichert

# Map mit Key = string Parameter (nach "/vehicles/<VIN>/domains/") und Value = (DB-Spaltenname, DB-Typ, [Faktor=1] )
# Keys aus weconnect.toJSON()
DATADEF = {
	"charging/batteryStatus/currentSOC_pct": ("Ladestand_pct", "smallint"),
	"charging/batteryStatus/cruisingRangeElectric_km": ("Reichweite_km", "smallint"),
	"charging/chargingStatus/remainingChargingTimeToComplete_min": ("Restliche_Ladezeit_min", "-smallint"),
	"charging/chargingStatus/chargingState": ("Ladestatus", "-varchar(50)"),
	"charging/chargingStatus/chargePower_kW": ("Ladeenergie_kW", "f"),
	"charging/chargingStatus/chargeRate_kmph": ("Laderate_km/h", "-f"),
	"charging/chargingSettings/targetSOC_pct": ("Ziel_Ladestand_pct", "smallint"),
	"charging/plugStatus/plugConnectionState": ("Ladekabelstatus", "-varchar(50)"),
	"climatisation/climatisationStatus/climatisationState": ("Klimastatus", "-varchar(50)"),
	"climatisation/climatisationSettings/targetTemperature_C": ("Klimatemperatur_gC", "-f"),
}

TABLENAME = "car"



class Car(Thread):
	"""Fahrzeug (per VW-WeConnect) auslesen.
	"""

	def __init__(self, carConfig: SectionProxy, stopEvent: Event, db: database.Database):
		"""Fahrzeug-Auslesen init."""
		Thread.__init__(self)
		self.name = "Car_" + self.name
		self._config = carConfig
		self._readInterval = max(2*60, self._config.getint("readInterval", fallback=5*60))	# at least 2min
		self._username = self._config.get("username")
		self._password = self._config.get("password")
		self._carVIN = self._config.get("carVIN")
		self._stopEvent = stopEvent
		self._database = db
		self.lastData = {}


	def _close(self):
		pass		# nothing to do here


	def run(self):
		LOG.info("Starting thread, using %s", self._username)
		try:
			weConnect = None
			while not self._stopEvent.is_set():
				try:
					if not weConnect:	# falls Login nicht klappt (Server unerreichbar), dann auch dies erneut probieren
						weConnect = weconnect.WeConnect(username=self._username, password=self._password,
								loginOnInit=True, updateAfterLogin=False, forceReloginAfter=20*60)
					weConnect.update(updateCapabilities=False, updatePictures=False, force=True)
					msgTime = utils.truncMs(datetime.now(), 0)
					fullText = weConnect.toJSON()
					#data = weConnect.vehicles[self._carVIN]		# liefert KeyError bei ungültiger VIN
					#fullText = str(data)
					#LOG.debug(fullText)

					self.lastData[database.COL_ZEIT] = msgTime
					cols = [database.COL_ZEIT, COL_TEXT]
					values = [msgTime, fullText]
					addressPrefix = "/vehicles/" + self._carVIN + "/domains/"
					for (code, ddTuple) in DATADEF.items():
						try:
							val = weConnect.getByAddressString(addressPrefix + code).value
							if isinstance(val, enum.Enum):		# z.B. ChargingState, ClimatisationState
								val = val.value
							self.lastData[ddTuple[0]] = val
							if not ddTuple[1].startswith("-"):
								cols.append(ddTuple[0])
								values.append(val)
						except:
							LOG.warning("Data-code '%s' not found/readable.", code)
					
					self._database.saveData(TABLENAME, cols, values)
					
					self._stopEvent.wait(timeout=self._readInterval)
				except:
					LOG.exception("Exception in Car readout loop, trying again in 20s")
					sleep(20)
			#end while

		finally:
			self._close()
			LOG.info("Ending thread")
	#end run()


	def beendeKlima(self) -> bool:
		# TODO
		return False
	
#end class



def printCreateTables():
	database.printCreateTable(TABLENAME, DATADEF, "Fahrzeug-Daten mit eher geringer Frequenz (~ 5-10 min).")
	print("!!! Es fehlt darin:  " + COL_TEXT + ", text")
