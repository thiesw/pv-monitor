import math
from datetime import date, time, datetime
from typing import Tuple

BREITE_RAD = math.radians(53.395)
LAENGE = 8.021


def setOrt(breitengrad: float, laengengrad: float):
	"Ort setzen, Werte in Grad"
	global BREITE_RAD, LAENGE
	BREITE_RAD = math.radians(breitengrad)
	LAENGE = laengengrad


def _sonnenDeklination(dayOfYear: int) -> float:
	"Deklination der Sonne in Radians"
	return 0.409526325277017 * math.sin(0.0169060504029192 * (dayOfYear - 80.0856919827619));

def _zeitDifferenz(deklination: float) -> float:
	"Dauer des halben Tagbogens in Stunden: Zeit von Sonnenaufgang (Höhe) bis zum höchsten Stand im Süden"
	return 12.0 * math.acos( (math.sin(math.radians(-50.0/60.0)) - math.sin(BREITE_RAD) * math.sin(deklination)) \
	                        / (math.cos(BREITE_RAD) * math.cos(deklination)))  / math.pi;

def _zeitGleichung(dayOfYear: int) -> float:
	"Differenz zwischen wahrer und mittlerer Sonnenzeit"
	return  -0.170869921174742 * math.sin(0.0336997028793971 * dayOfYear + 0.465419984181394) \
	       - 0.129890681040717 * math.sin(0.0178674832556871 * dayOfYear - 0.167936777524864);


def sonnenAufgangUntergang(datum: date, offsetMinutes: int = 0) -> Tuple[time, time]:
	"""Zeit des Sonnenauf- und -untergangs (nur auf ca. 1-3 Minuten genau) für das Datum in lokaler Zeit ermitteln.
	offsetMinutes sind ein Sicherheitsfaktor früher bzw. später.
	"""
	dayOfYear = datum.timetuple().tm_yday
	tzDiffHours = datetime(datum.year, datum.month, datum.day).astimezone().utcoffset().total_seconds() / 60 / 60
	zdiff = _zeitDifferenz(_sonnenDeklination(dayOfYear))
	zgl = _zeitGleichung(dayOfYear)
	zgl += LAENGE / 15.0 - tzDiffHours
	#aufgang = 12 - zdiff - zgl - LAENGE / 15.0 + tzDiffHours
	aufgang = 12 - zdiff - zgl - offsetMinutes/60
	untergang = 12 + zdiff - zgl + offsetMinutes/60
	std = int(aufgang)
	minute = int((aufgang - std) * 60)
	tAufgang = time(std, minute)
	std = int(untergang)
	minute = int((untergang - std) * 60)
	tUntergang = time(std, minute)
	return (tAufgang, tUntergang)
