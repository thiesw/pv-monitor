﻿-- Test-Abfragen der Daten

select * from gas order by zeit desc limit 100

--update gas set stand_m3 = round(stand_m3::decimal, 3)


select * from sensordata order by zeit desc limit 2000

select * from ehz_highfreq order by zeit desc limit 100
select * from ehz order by zeit desc limit 100
select * from ehz  where zeit <= date'2022-09-02'  order by zeit desc limit 1000
select * from ehz where zeit >= date'2023-01-01' order by zeit limit 100

select * from inverter_highfreq order by zeit desc limit 100
select * from inverter order by zeit desc limit 100
select * from inverter_daily order by datum desc limit 100

select * from inverter_strings order by 1

select * from wallbox order by zeit desc limit 100

select * from car order by zeit desc limit 100


-- Grafana:  $__timeGroup(zeit, '2m', 0) AS time
