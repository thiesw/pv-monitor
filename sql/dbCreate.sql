-----------------------------------------------------------
-- als Admin:

create user pv;

CREATE DATABASE photovoltaik
  WITH OWNER = pv
  TEMPLATE = template0
  ENCODING = 'UTF8'
;

-- für Auswertungen (z.B. Grafana)
create user pvreport password 'pvReport';



-----------------------------------------------------------
-- als User "pv":

create table inverter_strings (
	string_nr  smallint primary key,
	kuerzel  varchar(5) not null,
	anzahl_module  smallint not null,
	bemerkung varchar
);
comment on table inverter_strings is 'Informationen zu den Strings des WR';

-- Strings bei mir:  1: OS = Ost-sued (17),  2: ON = Ost-nord (17),  3: G = Gaube (8),  5: Cp = Carport (10),  7: W = West (19)
insert into inverter_strings values (1, 'OS', 17, 'Ost-Südhälfte');
insert into inverter_strings values (2, 'ON', 17, 'Ost-Nordhälfte');
insert into inverter_strings values (3, 'G', 8, 'Gaube');
insert into inverter_strings values (5, 'Cp', 10, 'Carport');
insert into inverter_strings values (7, 'W', 19, 'West');
commit;



CREATE TABLE ehz_highfreq (
	Zeit timestamptz  primary key
	, Momentanleistung_W  integer
	, Eigenverbrauch_W  integer
);
COMMENT ON TABLE ehz_highfreq IS 'eHZ (Zähler) Daten mit hoher Frequenz (~ 1 Hz).';

CREATE TABLE ehz (
	Zeit timestamptz  primary key
	, Netzbezug_Wh  integer
	, Einspeisung_Wh  integer
	, Netzfrequenz_Hz  real
	, Spannung_L1_V  real
	, Spannung_L2_V  real
	, Spannung_L3_V  real
	, Strom_L1_mA  integer
	, Strom_L2_mA  integer
	, Strom_L3_mA  integer
	, Phasenabweichung_Spannung_L2  smallint
	, Phasenabweichung_Spannung_L3  smallint
	, Phasenabweichung_Strom_L1  smallint
	, Phasenabweichung_Strom_L2  smallint
	, Phasenabweichung_Strom_L3  smallint
);
COMMENT ON TABLE ehz IS 'eHZ (Zähler) Daten mit normaler/mittlerer Frequenz (~ 10 - 60 s).';



CREATE TABLE inverter_highfreq (
	Zeit timestamptz  primary key
	, Leistung_AC_W  integer
	, Spannung_ON_V  real
	, Strom_ON_mA  integer
	, Spannung_OS_V  real
	, Strom_OS_mA  integer
	, Spannung_G_V  real
	, Strom_G_mA  integer
	, Spannung_Cp_V  real
	, Strom_Cp_mA  integer
	, Spannung_W_V  real
	, Strom_W_mA  integer
);
COMMENT ON TABLE inverter_highfreq IS 'Wechselrichter-Daten mit hoher Frequenz (~ 1 Hz).';

CREATE TABLE inverter (
	Zeit timestamptz  primary key
	, Leistung_DC_W  integer
	, Spannung_L1_V  real
	, Spannung_L2_V  real
	, Spannung_L3_V  real
	, Strom_L1_mA  integer
	, Strom_L2_mA  integer
	, Strom_L3_mA  integer
	, Blindleistung_AC_Var  integer
	, Netzfrequenz_Hz  real
	, Wirkungsgrad  real
	, Temperatur_intern_gC  real
	, Isolationswiderstand_kOhm  integer
	, Daily_energy_Wh  integer
);
COMMENT ON TABLE inverter IS 'Wechselrichter-Daten mit normaler/mittlerer Frequenz (~ 10 - 60 s).';

CREATE TABLE inverter_daily (
	Datum date  primary key
	, Peak_Leistung_am_Tag_AC_W  integer
	, Startup_time  timestamptz
	, Shutdown_time  timestamptz
	, Total_accumulated_energy_Wh  integer
);
COMMENT ON TABLE inverter_daily IS 'Wechselrichter-Daten mit täglicher Frequenz.';



CREATE TABLE wallbox (
	Zeit timestamptz  primary key
	, Status  smallint
	, Spannung_L1_V  smallint
	, Spannung_L2_V  smallint
	, Spannung_L3_V  smallint
	, Strom_L1_mA  smallint
	, Strom_L2_mA  smallint
	, Strom_L3_mA  smallint
	, Ladeleistung_W  smallint
	, Temperatur0_gC  real
	, Temperatur5_gC  real
);
COMMENT ON TABLE wallbox IS 'Wallbox Daten mit normaler/mittlerer Frequenz (~ 10 - 60 s).';


CREATE TABLE car (
	Zeit timestamptz  primary key
	, Ladestand_pct  smallint
	, Reichweite_km  smallint
	, Ladeenergie_kW  real
	, Ziel_Ladestand_pct  smallint
	, FullText text
);
COMMENT ON TABLE car IS 'Fahrzeug-Daten mit eher geringer Frequenz (~ 5-10 min).';


CREATE TABLE sensorData (
	Zeit timestamptz not null
	, Ort  varchar(50) not null
	, Temperatur  real
	, Luftfeuchtigkeit  real
	, primary key (Zeit, Ort)
);
COMMENT ON TABLE sensorData IS 'Sensor-Daten';

CREATE TABLE gas (
	Zeit timestamptz  primary key
	, Stand_m3  real
	, Ticks  integer
);
COMMENT ON TABLE gas IS 'Gas-Zählerstand';


-- muss nach jeder neu erstellten Tabelle erneut ausgeführt werden!
grant select on ALL TABLES IN SCHEMA public to pvreport;
